<?php
// a php script can start and stop in a file
// we can create & set variables, use $ to indicate a variable name
$dbHost = 'localhost';
$dbUser = 'root'; // bad security to use root!
$dbPW = 'usbw'; // worse security to not change root's default password!!
$dbName = 'test';
$dbTable = 'people'; 
// we can mix in plain html with php, and we can echo or print html using php
?>
<html>
	<head>
		<title>PHP Connection to MySQL Tests</title>
		<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
	</head>
	<body>
		<div id="content">
			<div>
				<h1>PHP Connection to MySQL Test2 (Using msqli_ commands) Object Oriented style</h1>
				<h3>Display table data in a table.</h3>
				<p>
				<?php		
				echo 'Step 1: Connecting to test mySQL database<br />';
				// connect to just the mySQL server this time
				$mysqli = new mysqli($dbHost, $dbUser, $dbPW, $dbName);
				/* check connection */
				if ($mysqli->connect_errno) {
					printf("Connect failed: %s\n", $mysqli->connect_error);
					exit();
				}
				echo ("<br />Set character set, to show its impact on some values (e.g., length in bytes)<br />");
				foreach (array('latin1', 'utf8') as $charset) {

					// Set character set, to show its impact on some values (e.g., length in bytes)
					$mysqli->set_charset($charset);
					$query = "Select * FROM " . $dbTable;
					echo "======================<br />\n";
					echo "Character Set: ".$charset."<br />\n";
					echo "======================<br />\n";
					if ($result = $mysqli->query($query)){
					/* Get field information for all columns */
						$finfo = $result->fetch_fields();

						foreach ($finfo as $val) {
							printf("Name:      %s<br />\n",   $val->name);
							printf("Table:     %s<br />\n",   $val->table);
							printf("Max. Len:  %d<br />\n",   $val->max_length);
							printf("Length:    %d<br />\n",   $val->length);
							printf("charsetnr: %d<br />\n",   $val->charsetnr);
							printf("Flags:     %d<br />\n",   $val->flags);
							printf("Type:      %d<br />\n\n", $val->type);
						}						
					}
				}
				?>
				</p>
				<table border="0" cellpadding="3" width="600">
				<tr class="h">
				<?php
				// ask mysql for the field information and place the name in the table as headers
				foreach ($finfo as $val) {
						echo ("<th>{$val->name}</th>");
				}
				?>
				</tr>
				<?php		
				// display or print the data in the table
				// While iterates through the rows
					if ($result = $mysqli->query($query))
					{
						while($row = $result->fetch_row()){					
							echo "<tr>";
							// Foreach iterates through the columns
							foreach($row as $_column) {
								echo ("<td class='e'>{$_column}</td>");
							}
							echo "</tr>\n";
						}
					}
				?>

				</table>	
				</p>
				<p>
				Step 4:
				Disconnect from the database
				</p>
				<?php
					//Step 4
					//$result->free();
					$mysqli->close();// close the database connection
				?>
				
			</div>

		</div>
	 </body>	
</html>				