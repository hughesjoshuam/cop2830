<?php

	/*MYSL Connection String*/
	$servername = "localhost";
	$username = "usrMyWebApp";
	$password = "psw_MyWebApp";
	
	// Create connection
	$connection = new mysqli($servername, $username, $password);

	// Database maniuplation logic
	if(!empty($_POST)){
		if(!empty($_POST["strLastName"]) && !empty($_POST["strFirstName"])){
			//Insert
			if($_POST["strFirstName"] != "" || $_POST["strLastName"] != "" || $_POST["strFirstName"] != null || $_POST["strLastName"] != null){
				//Generate query string based on form submissions
				$strQuery = sprintf('INSERT INTO `dbMyWebApp`.`tblMyWebAppTable` (`FirstName`, `LastName`, `MiddleInitial`, `Birthday`, `PhoneNumber`, `Major`) 
				VALUES
				("%s","%s","%s","%s","%s","%s");',
				$_POST["strFirstName"],
				$_POST["strLastName"],
				$_POST["strMiddleInitial"],
				$_POST["strBirthday"],
				$_POST["strPhoneNumber"],
				$_POST["strMajor"]);
				
				//Insert information into database
				$connection->query($strQuery);
			}
		}
			
		if(!empty($_POST["deleteID"])){
			//Delete
			if($_POST["deleteID"] != ""){
				$strQuery = sprintf('DELETE FROM `dbMyWebApp`.`tblMyWebAppTable` WHERE id = %s', $_POST["deleteID"]);
				$connection->query($strQuery);
			}
		}
	}	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>PHP Hello World</title>
		<meta encoding="UTF-8"/>
		
	</head>
	<body>
		<form name="phpHelloForm" action="hello.php" method="POST">
			Enter your First Name: <input type="text" name="strFirstName"><br />
			Enter your Last Name: <input type="text" name="strLastName"><br />
			Enter your Middle Initial: <input type="text" name="strMiddleInitial"><br />
			Enter your Birthday (yyyy-mm-dd): <input type="text" name="strBirthday"><br />
			Enter your PhoneNumber: <input type="text" name="strPhoneNumber"><br />
			Enter your Major: <input type="text" name="strMajor"><br />
			<button type="submit">Add Entry</button>
		</form>	
		<form name="phpDatabaseList" action="hello.php" method="POST">
			<?php
				// Query database for existing entries
				$strQuery = "Select * from `dbMyWebApp`.`tblMyWebAppTable`";
				$strResults = $connection->query($strQuery);

				// Set Static Statements
				$strMyStatement1 = 'Hello';
				$strMyStatement2 = 'how are you doing?';
				
				if ($strResults->num_rows > 0) {
					// output data of each row
					while($row = $strResults->fetch_assoc()) {
						printf('<button type="submit" onclick="javascript: this.form.deleteID.value = %s;">Delete</button>%s: %s %s %s %s %s - Birthday: %s - PhoneNumber: %s<br />', 
						$row["id"],
						$row["Major"], 
						$strMyStatement1, 
						$row["FirstName"],
						$row["MiddleInitial"],
						$row["LastName"], 
						$strMyStatement2,
						$row["Birthday"],
						$row["PhoneNumber"]);
					}
				}	
			?>
			<input type="hidden" name="deleteID" value="" />
		</form>
	</body>
</html>
