<?php
include 'mysql.cfg'; //moved login information into include configuration file.
// we can mix in plain html with php, and we can echo or print html using php

if(isset($_POST['sqlType'])){
	$sqlFormat = $_POST['sqlType'];  
} else {
	$sqlFormat = ''; 
}

?>
<html>
	<head>
		<title>PHP Connection to MySQL Tests</title>
		<link href="style2.css" rel="stylesheet" type="text/css" media="screen" />
	</head>
	<body>
		<div id="content">
			<!--Set to mysql or mysqli will change which sql connectivity example is displayed. -->
			<form name="frmSqlType" method="POST">
			Select SQL Format	<select name="sqlType" onchange="javascript: this.form.submit(); ">
					<option value="">Select Value</option>
					<option value="mysql" <?php if($sqlFormat == 'mysql') echo 'selected = "selected"'; ?>>mysql</option>
					<option value="mysqli" <?php if($sqlFormat == 'mysqli') echo 'selected = "selected"'; ?>>mysqli</option>
					</select>
			</form>
			<?php if($sqlFormat == 'mysqli'){ //If sqlFormat  mysql display this section; otherwise hide. ?>
				<div>
					<h1>PHP Connection to MySQL Test1 (Using msqli_ commands)</h1>
					<h2> $sqlFormat is set to "mysqli" </h2>
					<h3>Display table data in an unordered list.</h3>
					<p>
					<?php		
					echo 'Step 1: Connecting to test mySQL database';
					$db = mysqli_connect($dbHost, $dbUser, $dbPW, $dbName)
					or die('Error connecting to MySQL server ' + mysqli_error());
					
					//$db = mysqli_connect('localhost', 'root', 'usbw', 'my_guitar_shop1')
					
					?>
					</p>
					<p>
					<?php		
					echo 'Step 2: Read some data from the test mySQL database<br />';
					$query = "Select * FROM " . $dbTable ; //$dbTable;
					$result = mysqli_query($db, $query) or die('Error querying database.  ' + mysqli_error());				
					?>
					</p>
					<p>
					Step 3: Display that data		
					<ul>
					<?php	
					//Updated code to match the original table that I created.	
					while ($row = mysqli_fetch_array($result)) {
						echo '<li>' . $row['id']. ' ' . $row['FirstName']. ' ' . $row['MiddleInitial']. ' ' . $row['LastName'] . ' ' . $row['Major'] . '</li>';
						//echo '<li>' . $row['productName']. ' ' . $row['categoryID'] . ' $' . $row['listPrice'] . '</li>';
					}
					?>
					</ul>	
					<p>
					Step 4:
					Disconnect from the database
					</p>
					<?php
						//Step 4
						mysqli_close($db);  // close the database connection
					?>
				</div>
			<?php } ?>
			<?php if($sqlFormat == 'mysql'){ //If sqlFormat  mysql display this section; otherwise hide. ?>
				<div>
					<h1>PHP Connection to MySQL Test2 (Using msql_ commands)</h1>
					<h2> $sqlFormat is set to "mysql" </h2>
					<h3>Display table data in a table.</h3>
					<p>
					<?php	
					echo 'Step 1: Connecting to test mySQL database';
					// connect to just the mySQL server this time
					$db = mysql_connect($dbHost, $dbUser, $dbPW)
					or die('Error connecting to MySQL server. ' + mysql_error());
					// select the specific database
					mysql_select_db($dbName, $db) or die('Error selecting database. ' + mysql_error());
					
					?>
					</p>
					<p>
					<?php		
					echo 'Step 2: Read some data from the test mySQL database<br />';
					//$query = "Select * FROM " . $dbTable ; //$dbTable;
					$query = "SELECT id, LastName, FirstName, MiddleInitial, Birthday, PhoneNumber, Major, Address1, Address2, City, State, Zip, HairColor, EyeColor, Height
								FROM tblmywebapptable, tblpersondetails
								WHERE `tblpersondetails`.`mywebapptableid` = `tblmywebapptable`.`id`
								ORDER BY `tblmywebapptable`.`LastName`, `tblmywebapptable`.`FirstName`"; //$dbTable;
					$result = mysql_query($query) or die('Error querying database. ' + mysql_error());				
					?>
					</p>
					<p>
					Step 3: Display that data in a table.		
					<table border="0" cellpadding="3" width="600">
					<tr class="h">
					<?php
					// ask mysql for the field information and place the name in the table as headers
					for($i=0;$i<mysql_num_fields($result);$i++)
					{
							$field_info = mysql_fetch_field($result, $i);

							if($field_info->name == 'id')
							{
								echo ("<th>Person ID</th>");
							}
							else
							{
								echo ("<th>{$field_info->name}</th>");
							}
							
					}
					?>
					</tr>
					<?php		
					// display or print the data in the table
					// While iterates through the rows
					while ($row = mysql_fetch_row($result)) {
						// Foreach iterates through the columns
						foreach($row as $field) {
							//use an index to track the current column indicator. Need to identify date column by number.
							if(!isset($index)){
								$index = 0; 
							}
							if($index == 4){
								date_default_timezone_set('EST');
								$birthDate = date_create_from_format("Y-m-d", $field);
								$birthDate = date_format($birthDate, "m/d/Y"); 
								echo ("<td class='e'>{$birthDate}</td>");
							}else{
								echo ("<td class='e'>{$field}</td>");
							}
							
							$index++; //increase the index
						}
						echo "</tr>";
						//Reset the index
						$index = 0;
					}
					?>
					</table>	
					<p>
					Step 4:
					Disconnect from the database
					</p>
					<?php
						//Step 4
						mysql_close($db);  // close the database connection
					?>
					
				</div>
			<?php } ?>
			<div>
			</div>
		</div>
	 </body>
	
</html>