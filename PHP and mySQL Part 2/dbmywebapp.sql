-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 16, 2017 at 09:32 PM
-- Server version: 5.6.13
-- PHP Version: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbmywebapp`
--
CREATE DATABASE IF NOT EXISTS `dbmywebapp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dbmywebapp`;

-- --------------------------------------------------------

--
-- Table structure for table `tblmywebapptable`
--

CREATE TABLE IF NOT EXISTS `tblmywebapptable` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Primary Key',
  `FirstName` text COMMENT 'FirstName',
  `LastName` text COMMENT 'LastName',
  `MiddleInitial` text COMMENT 'Middle Initial',
  `Birthday` date DEFAULT '0000-00-00' COMMENT 'BirthDate',
  `PhoneNumber` varchar(20) DEFAULT '' COMMENT 'PhoneNumber',
  `Major` varchar(250) DEFAULT NULL COMMENT 'Course Major',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `PhoneNumber` (`PhoneNumber`),
  FULLTEXT KEY `Major` (`Major`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tblmywebapptable`
--

INSERT INTO `tblmywebapptable` (`id`, `FirstName`, `LastName`, `MiddleInitial`, `Birthday`, `PhoneNumber`, `Major`) VALUES
(10, 'Joshua', 'Hughes', 'M', '1980-03-09', '941-740-2072', 'Computer Programming and Analysis'),
(11, 'John', 'Doe', 'J.', '1960-01-01', '123-456-7890', 'Psychology'),
(12, 'Jane', 'Doe', 'J.', '1960-01-01', '098-765-4321', 'Math');

-- --------------------------------------------------------

--
-- Table structure for table `tblpersondetails`
--

CREATE TABLE IF NOT EXISTS `tblpersondetails` (
  `PersonDetailsID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PersonDetailsID - Primary Key',
  `myWebAppTableID` int(11) NOT NULL COMMENT 'Foreign Key Constraint',
  `Address1` varchar(250) DEFAULT NULL COMMENT 'Address 1',
  `Address2` varchar(250) DEFAULT NULL COMMENT 'Address 2',
  `City` text COMMENT 'City',
  `State` text COMMENT 'State',
  `zip` text COMMENT 'Zip',
  `HairColor` text COMMENT 'Hair Color',
  `EyeColor` text COMMENT 'Eye Color',
  `Height` varchar(10) DEFAULT NULL COMMENT 'Height',
  PRIMARY KEY (`PersonDetailsID`),
  UNIQUE KEY `myWebAppTableID` (`myWebAppTableID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tblpersondetails`
--

INSERT INTO `tblpersondetails` (`PersonDetailsID`, `myWebAppTableID`, `Address1`, `Address2`, `City`, `State`, `zip`, `HairColor`, `EyeColor`, `Height`) VALUES
(1, 10, '32524 Oil Well Road', NULL, 'Punta Gorda', 'Florida', '33955', 'Brown/Gray', 'Blue', '5''11"'),
(2, 11, '1234 Mocking Bird Lane', 'Suite: 203', 'Holywood', 'California', '12345', 'Purple', 'Green', '6''5"'),
(3, 12, '5050 Court Street', 'Floor: 3 Room: 525', 'UpTown', 'Funk', '54321', 'brown', 'blue', '4''3"');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
