/*
   New Perspectives on HTML5, CSS, and JavaScript
   Tutorial 14
   Case Problem 4

   Author: Amy Wu
   Date:   3/1/2015

   Filename: comments.js


*/

window.onload = init;

function init() {
   document.getElementById("addReview").onclick = addReview;
}

function addReview() {
   var reviewsBox = document.getElementById("reviews");
   var bqElement = document.createElement("blockquote");
   var p1Element = document.createElement("p");

   for (var i = 1; i <= document.getElementById("stars").value; i++) {
      var starImg = document.createElement("img");
      starImg.src = "star.png";
      p1Element.appendChild(starImg);
   }

   bqElement.appendChild(p1Element);

   var p2Element = document.createElement("p");
   p2Element.innerHTML = document.getElementById("comment").value;
   bqElement.appendChild(p2Element);

   var p3Element = document.createElement("p");
   p3Element.innerHTML = "&mdash; ";
   p3Element.innerHTML += document.getElementById("username").value;
   bqElement.appendChild(p3Element);

   var p4Element = document.createElement("p");
   var thisTime = new Date();
   p4Element.innerHTML += thisTime.toLocaleString();
   bqElement.appendChild(p4Element);

   reviewsBox.insertBefore(bqElement, reviewsBox.firstChild);
}