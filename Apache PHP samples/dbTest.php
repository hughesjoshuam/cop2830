<?php
// a php script can start and stop in a file
// we can create & set variables, use $ to indicate a variable name
$dbHost = 'localhost';
$dbUser = 'root'; // bad security to use root!
$dbPW = 'usbw'; // worse security to not change root's default password!!
$dbName = 'test';
$dbTable = 'people'; 
// we can mix in plain html
?>
<html>
	<head>
		<title>PHP Connection to MySQL Test</title>
	</head>
	<body>
		<h1>PHP Connection to MySQL Test</h1>
		<p>
		<?php		
		echo 'Step 1: Connecting to test mySQL database';
		//$db = mysqli_connect('localhost', 'root', 'usbw', 'test')
		$db = mysqli_connect('localhost', 'root', 'usbw', 'my_guitar_shop1')
		or die('Error connecting to MySQL server');
		?>
		</p>
		<p>
		<?php		
		echo 'Step 2: Read some data from the test mySQL database<br />';
		$query = "Select * FROM " . "products" ; //$dbTable;
		$result = mysqli_query($db, $query) or die('Error querying database');				
		?>
		</p>
		<p>
		Step 3: Display that data		
		<ul>
		<?php		
		while ($row = mysqli_fetch_array($result)) {
			//echo '<li>' . $row['id']. ' ' . $row['person_name'] . ' ' . $row['person_description'] . '</li>';
			echo '<li>' . $row['productName']. ' ' . $row['categoryID'] . ' $' . $row['listPrice'] . '</li>';
		}
		?>
		</ul>
		
	</body>
	<p>
	Step 4:
	Disconnect from the database
	</p>
	<?php
		//Step 4
		mysqli_close($db);  // close the database connection
	?>
</html>