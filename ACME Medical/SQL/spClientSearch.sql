DELIMITER //
CREATE PROCEDURE spClientSearch
(IN strFirstName VARCHAR(250), 
IN strLastName VARCHAR(250),
IN dtBirthDate Date,
IN strSSN VARCHAR(11),
IN intID int)
BEGIN
	SET @strQuery = 'SELECT * FROM vclients WHERE ';
  	SET @strQuery = @strQuery + IF(`strFirstName` != "" AND `strFirstName` != NULL, "firstname = `strFirstName`","");
  	PREPARE stmt FROM @sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END //
DELIMITER ;