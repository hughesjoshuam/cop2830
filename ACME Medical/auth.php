<?php 

// Start browser session
session_start();

// Include database and connect to database to verify credentials
include 'Config/database.cfg';
$dbConnection = mysql_connect($dbServer, $dbUserName, $dbPassword)
or die('Error connecting to MySQL server ' + mysql_error());

// select the specific database
mysql_select_db($dbName, $dbConnection) 
or die('Error selecting database. ' + mysql_error());
					
// Create query and test credentials against database results
$strQuery = sprintf("Select * from tblusers where username = '%s' and password = '%s'",
	$_POST['username'],
	$_POST['password']);
	
$result = mysql_query($strQuery, $dbConnection) or die('Query Failed: '.mysql_error());
$resultSet = mysql_fetch_array($result);

if (mysql_num_rows($result) > 0){
	
	// Set session variables
	
	$_SESSION['Name'] = $resultSet['firstName']." ".$resultSet['lastName'];
	$_SESSION['UserID'] = $resultSet['id'];
	$_SESSION['Email'] = $resultSet['email'];
	$_SESSION['Default'] = "clientSearch";
	$_SESSION['Login'] = true;
	
	// redirect to index.php for application processing
    header('Location: index.php');
	
} else {
	
	// No user results matched entered criteria. Redirect to login with failed login message.
	$login_comment = 'Login Failed';
	
	include 'header.php';
	include 'login.php';
	include 'footer.php';
}
?>