<?php
/*
Joshua M. Hughes
COP2830
Professor Barrell
2/19/2017

    Primary Application Logic File

*/

/* USBWebServer cannot perform https -- at least not easily
//Check for SSL and require HTTPS protocol
if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS']=='off'):
	$redirectURL='https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
	HEADER("LOCATION:".$redirectURL);
endif;
*/

// Initialize browser Session
if (!isset($_SESSION['Login'])){
    session_start();
}

//Initialize database connectivity
require_once 'Config/database.cfg';
/*
[Replacing with code for PDO connection]

$dbConnection = mysql_connect($dbServer, $dbUserName, $dbPassword)
or die('Error connecting to MySQL server ' + mysql_error());

// select the specific database
mysql_select_db($dbName, $dbConnection) 
or die('Error selecting database. ' + mysql_error());

*/
try{
	$dsn = 'mysql:host=' . $dbServer . ';dbname=' . $dbName;
	$dbConnection = new PDO($dsn, $dbUserName, $dbPassword);
}catch (PDOException $e){
	$error = $e->getMessage();
	die('Error connecting to MySQL server ' + $error);
}

// Check if requested app is set, or load session default app.
if (isset($_GET['app'])){
	$app = $_GET['app'];
} else if (isset($_SESSION['Default'])){
	$app = $_SESSION['Default'];
}

if (isSet($app)){
	// check security to ensure established session and login TRUE
	include 'security.php';
	
    // include page header
    include 'header.php';
    
    // include page navigation
    include 'navigation.php';
    
    // display application module
    if ($app == 'clientSearch'){
    	include 'ClientSearch/controllerClientSearch.php';
		
	}
	
    if ($app == 'clientDemographics'){
		include 'Demographics/controllerDemographics.php';
		
	}
	
    if ($app == 'clinicalVisit'){
		include 'ClinicalVisit/controllerClinicalVisit.php';
		
	}
	if ($app == 'clientVitals'){
		include 'Vitals/controllerVitals.php';
		
	}
    if ($app == 'reportPanel'){
		include 'Reports/controllerReports.php';
		
	}
    if ($app == 'adminPanel'){
		include 'Admin/controllerAdmin.php';
		
	}
    if ($app == 'logOut'){
    	session_destroy();
    	header('Location: index.php');
    	exit();
    	
	}
    
    // include page footer
	include 'footer.php';

} else {
	// include page header
    include 'header.php';
	
	// app is not set, session is not established, send to  login
	include 'login.php'; 
	
	// include page footer
	include 'footer.php';
	
}
?>