<?php
/* 
* View: Admin Panel
* Author: Joshua M. Hughes
* Modified: 03/16/2017
* COP2830
* Professor Barrell
*/
?>
<h3>Administration Panel</h3>
<h4>User Administration</h4>
Add User<br />
Delete User<br />
Edit User<br />
<br />
<h4>Application Settings</h4>
Business Title<br />
Business Phone<br />
Business Address<br />
<br />
<h4>Clinical Visit</h4>
Purge Clinical Visit Data<br />
<br />
