<?php
/* 
* Model: Client Search
* Author: Joshua M. Hughes
* Modified: 03/02/2017
* COP2830
* Professor Barrell
*/

	// Convert post scope variables to local scope variable
	if(isset($_POST)){
		foreach($_POST as $key => $value){
			//${'str' . $Key} = $value;
			$$key = $value;
		}
	}
	
	// Begin assembling query statement
	$strQuery = "Select * from vclients where 1=1 ";
	
	// Add firstname to search query
	if(isset($FirstName) && $FirstName != ''){
		$strQuery = sprintf("%s and firstname = '%s' ",
		$strQuery,
		$FirstName);
	}
	// Add last name to search query
	if(isset($LastName) && $LastName != ''){
		$strQuery = sprintf("%s and lastname = '%s' ",
		$strQuery,
		$LastName);
	}
	// Add birthdate to search query
	if(isset($BirthDate) && $BirthDate != ''){
		$strQuery = sprintf("%s and birthday = '%s' ",
		$strQuery,
		$BirthDate);
	}
	// Add Medical Record Number to search uery
	if(isset($MRN) && $MRN != ''){
		$strQuery = sprintf("%s and id = '%s' ",
		$strQuery,
		$MRN);	
	}
	
	// Initialize MySQL PDO object
	$statement = $dbConnection->prepare($strQuery);
	$statement->execute();
	$result = $statement->fetchAll();
	$statement->closeCursor();
	
	//$result = mysql_query($strQuery, $dbConnection) or die('Query Failed: '.mysql_error());
?>