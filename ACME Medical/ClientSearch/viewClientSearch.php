<?php
/* 
* View: Client Search
* Author: Joshua M. Hughes
* Modified: 03/02/2017
* COP2830
* Professor Barrell
*/

?>
<h3>Client Search</h3>
<div id="ClientSearch">
	<form name="frmClientSearch" action="index.php?app=clientSearch" method="post">
		First name: <input type="text" name="FirstName" value="<?php echo (isset($FirstName)) ? $FirstName : ''; ?>" /> &nbsp;&nbsp; Last name: <input type="text" name="LastName" value="<?php echo (isset($LastName)) ? $LastName : ''; ?>" /> 
		<br />
		<br />
		Birth Date: <input type="date" name="BirthDate" value="<?php echo (isset($BirthDate)) ? $BirthDate : ''; ?>" /> &nbsp;&nbsp; Social Security Number <input type="text" name="SSN" value="<?php echo (isset($SSN)) ? $SSN : ''; ?>" />
		<br />
		<br />
		Medical Record Number: <input type="text" name="MRN" value="<?php echo (isset($MRN)) ? $MRN : ''; ?>" /><br />
		<br />
		<br />
		<input type="hidden" name="SearchAction" value="true" />
		<button type="submit">Search</button> &nbsp; &nbsp; <button type="reset">Clear</button><br />
		<br />
	</form>
</div>
<div id="ClientSearchResults">
	<?php 
		if(count($result) > 0){
			if(isset($SearchAction) && $SearchAction == 'true'){ ?>
	<table name="ClientSearchResults">
			<tr>
				<th>MRN</th>
				<th>First Name</th>
				<th>Middle Name</th>
				<th>Last Name</th>
				<th>Birth Date</th>
				<th>Social Security Number</th>
			</tr>
	<?php foreach($result as $row){ ?>
		<tr class="resultRow" 
			onclick="javascript: window.location='index.php?app=clientDemographics&MRN=<?php echo $row['id']; ?>'">
			<td class="resultRow"><?php echo $row['id']; ?></td>	
			<td class="resultRow"><?php echo $row['FirstName']; ?></td>
			<td class="resultRow"><?php echo $row['MiddleName']; ?></td>
			<td class="resultRow"><?php echo $row['LastName']; ?></td>
			<td class="resultRow"><?php echo $row['Birthday']; ?></td>
			<td class="resultRow"><?php echo $row['SSN']; ?></td>
		</tr>
		</a>
		<?php }	} }	?>
	</table>	
</div>