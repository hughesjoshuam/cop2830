<?php 
/* Joshua M. Hughes
 * COP2830
 * 3/30/2017
 * Professor Barrell
 * 
 * index.php file
 * Chapter 7 Murach PHP & MySQl
 */
include 'header.php'; 

$credit_card = filter_input(INPUT_GET, 'credit_card');
$action = filter_input(INPUT_GET, 'action');
$P = isset($_GET['P']);
$H = isset($_GET['H']);
$S = isset($_GET['S']);
$K = isset($_GET['K']);
$B = isset($_GET['B']);
$U = isset($_GET['U']);
$M = isset($_GET['M']);
$O = isset($_GET['O']);
$G = isset($_GET['G']);
$R = isset($_GET['R']);
$Z = isset($_GET['Z']);
$J = isset($_GET['J']);
$T = isset($_GET['T']);
$Y = isset($_GET['Y']);
$Q = isset($_GET['Q']);

?>

<main>      
    <h1>Chapter 7 - In Class Walk Through</h1>
    <!-- Display a list of forms -->
    <h2>Forms</h2>
    <section>
        <div>
            <p>
                <form method="GET">
                    <p>Card Type: <?php echo $credit_card; ?></p>
                    <p>Toppings: <?php 
                            $topping = "";
                            $divider = ", ";
                            // Process topping values
                            if($P == true){
                                $topping = $topping . $divider . "Pepperoni"; 
                            }
                            if($H){
                                $topping =  $topping . $divider . "Ham"; 
                            }
                            if($S){
                                $topping =  $topping . $divider . "Sausage"; 
                            }
                            if($K){
                                $topping =  $topping . $divider . "Bacon"; 
                            }
                            if($B){
                                $topping =  $topping . $divider . "Beef"; 
                            }
                            if($U){
                                $topping =  $topping . $divider . "Chicken"; 
                            }
                            if($M){
                                $topping =  $topping . $divider . "Mushroom"; 
                            }
                            if($O){
                                $topping =  $topping . $divider . "Onion"; 
                            }
                            if($G){
                                $topping =  $topping . $divider . "Green Pepper"; 
                            }
                            if($R){
                                $topping =  $topping . $divider . "Black Olive"; 
                            }
                            if($Z){
                                $topping =  $topping . $divider . "Banana Pepper"; 
                            }
                            if($T){
                                $topping =  $topping . $divider . "Tomato"; 
                            }
                            if($J){
                                $topping =  $topping . $divider . "Jalapeno"; 
                            }
                            if($Q){
                                $topping =  $topping . $divider . "BBQ"; 
                            }
                            if($Y){
                                $topping =  $topping . $divider . "Cheddar Cheese"; 
                            }                            
                            ($topping == "") ? print("None") : print $topping;                       
                        ?>
                    </p>
                    <p>Action: <?php echo $action; ?>
                </form>
            </p>
        </div>
    </section>
</main>
<?php include 'footer.php'; ?>