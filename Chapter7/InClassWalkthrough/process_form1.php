<?php 
/* Joshua M. Hughes
 * COP2830
 * 3/30/2017
 * Professor Barrell
 * 
 * index.php file
 * Chapter 7 Murach PHP & MySQl
 */
include 'header.php'; 

$user_name = filter_input(INPUT_GET, 'user_name');
$password = filter_input(INPUT_GET, 'password');
$action = filter_input(INPUT_GET, 'action');
?>

<main>      
    <h1>Chapter 7 - In Class Walk Through</h1>
    <!-- Display a list of forms -->
    <h2>Forms</h2>
    <section>
        <div>
            <p>
                <form method="GET">
                    <p>User Name: <?php echo $user_name; ?></p>
                    <p>Password: <?php echo $password; ?></p>
                    <p>Action: <?php echo $action; ?>
                </form>
            </p>
        </div>
    </section>
</main>
<?php include 'footer.php'; ?>