<?php 
/* Joshua M. Hughes
 * COP2830
 * 3/30/2017
 * Professor Barrell
 * 
 * index.php file
 * Chapter 7 Murach PHP & MySQl
 */
include 'header.php'; 

?>

<main>      
    <h1>Chapter 7 - In Class Walk Through</h1>
    <!-- Display a list of forms -->
    <h2>Forms</h2>
    <section>
        <div>
            <p>
            <h3>Form 1 - GET</h3>
                <form name="form1" method="GET" action="process_form1.php">
                    <p>User Name:
                        <input type="text" name="user_name" value="rharris" />
                    </p>
                    <p>Password:
                        <input type="password" name="password" />
                    </p>
                    <input type="hidden" name="action" value="login" />
                    <p>
                        <input type="submit" value="Login" />
                    </p>
                </form>
            </p>
        </div>
        <div>
            <p>
            <h3>Form 2 - GET</h3>
                <form name="form2" method="GET" action="process_form2.php">
                    <p>
                        Credit Card: <br />
                        <input type="radio" name="credit_card" value="visa" checked="checked" /> Visa<br />
                        <input type="radio" name="credit_card" value="Mastercard" /> Mastercard<br /> 
                        <input type="radio" name="credit_card" value="American Express" /> American Express<br /> 
                        <input type="radio" name="credit_card" value="Discover" /> Discover<br /> 
                        <input type="radio" name="credit_card" value="Diners Club" /> Diners Club<br /> 
                        <input type="radio" name="credit_card" value="Walmart Card" /> Walmart Card<br /> 
                        <input type="radio" name="credit_card" value="Gift Card" /> Gift Card<br /> 
                        <input type="radio" name="credit_card" value="PayPal Credit" /> PayPal Credit<br /> 
                    </p>
                    <p>
                        Select Pizza Topping(s): <br />
                        <input type="checkbox" name="P" checked="checked" /> Pepperoni<br />
                        <input type="checkbox" name="H" /> Ham<br />
                        <input type="checkbox" name="S" /> Sausage<br />
                        <input type="checkbox" name="K" /> Bacon<br />
                        <input type="checkbox" name="B" /> Beef<br />
                        <input type="checkbox" name="U" /> Chicken<br />
                        <input type="checkbox" name="M" /> Mushroom<br />
                        <input type="checkbox" name="O" /> Onion<br />
                        <input type="checkbox" name="G" /> Green Pepper<br />
                        <input type="checkbox" name="R" /> Black Olives<br />
                        <input type="checkbox" name="Z" /> Banana Pepper<br />
                        <input type="checkbox" name="J" /> Jalapeno<br />
                        <input type="checkbox" name="T" /> Tomato<br />
                        <input type="checkbox" name="Y" /> Cheddar Cheese<br />
                        <input type="checkbox" name="Q" /> BBQ<br />                        
                    </p>
                    <input type="hidden" name="action" value="login" />
                    <p>
                        <input type="submit" value="Order Pizza" />
                    </p>
                </form>
            </p>
        </div>
        <div>
            <p>
            <h3>Form 3 - POST</h3>
                <form name="form3" method="post" action="process_form3.php">
                    <p>
                        Credit Card: <br />
                        <input type="radio" name="credit_card" value="visa" checked="checked" /> Visa<br />
                        <input type="radio" name="credit_card" value="Mastercard" /> Mastercard<br /> 
                        <input type="radio" name="credit_card" value="American Express" /> American Express<br /> 
                        <input type="radio" name="credit_card" value="Discover" /> Discover<br /> 
                        <input type="radio" name="credit_card" value="Diners Club" /> Diners Club<br /> 
                        <input type="radio" name="credit_card" value="Walmart Card" /> Walmart Card<br /> 
                        <input type="radio" name="credit_card" value="Gift Card" /> Gift Card<br /> 
                        <input type="radio" name="credit_card" value="PayPal Credit" /> PayPal Credit<br /> 
                    </p>
                    <p>
                        Select Pizza Topping(s): <br />
                        <input type="checkbox" name="topping[]" value="P" checked="checked" /> Pepperoni<br />
                        <input type="checkbox" name="topping[]" value="H" /> Ham<br />
                        <input type="checkbox" name="topping[]" value="S" /> Sausage<br />
                        <input type="checkbox" name="topping[]" value="K" /> Bacon<br />
                        <input type="checkbox" name="topping[]" value="B" /> Beef<br />
                        <input type="checkbox" name="topping[]" value="U" /> Chicken<br />
                        <input type="checkbox" name="topping[]" value="M" /> Mushroom<br />
                        <input type="checkbox" name="topping[]" value="O" /> Onion<br />
                        <input type="checkbox" name="topping[]" value="G" /> Green Pepper<br />
                        <input type="checkbox" name="topping[]" value="R" /> Black Olives<br />
                        <input type="checkbox" name="topping[]" value="Z" /> Banana Pepper<br />
                        <input type="checkbox" name="topping[]" value="J" /> Jalapeno<br />
                        <input type="checkbox" name="topping[]" value="T" /> Tomato<br />
                        <input type="checkbox" name="topping[]" value="Y" /> Cheddar Cheese<br />
                        <input type="checkbox" name="topping[]" value="Q" /> BBQ<br />                        
                    </p>
                    <input type="hidden" name="action" value="login" />
                    <p>
                        <input type="submit" value="Order Pizza" />
                    </p>
                </form>
            </p>
        </div>
    </section>
</main>
<?php include 'footer.php'; ?>