<?php 
/* Joshua M. Hughes
 * COP2830
 * 3/30/2017
 * Professor Barrell
 * 
 * index.php file
 * Chapter 7 Murach PHP & MySQl
 */
include 'header.php'; 

$credit_card = filter_input(INPUT_POST, 'credit_card');
$toppings = filter_input(INPUT_POST, 'topping', FILTER_SANITIZE_SPECIAL_CHARS, FILTER_REQUIRE_ARRAY);
$action = filter_input(INPUT_POST, 'action');

?>

<main>      
    <h1>Chapter 7 - In Class Walk Through</h1>
    <!-- Display a list of forms -->
    <h2>Forms</h2>
    <section>
        <div>
            <p>
                <form method="GET">
                    <p>Card Type: <?php echo $credit_card; ?></p>
                    <p>Toppings: <?php 
                            $topping = "";
                            $divider = ", ";
                            foreach($toppings as $value){
                                // Process topping values
                                switch ($value) {
                                    case "P":
                                        $topping = $topping . $divider . "Pepperoni"; 
                                        break;
                                    case "H":
                                        $topping = $topping . $divider . "Ham"; 
                                        break;
                                    case "S":
                                        $topping = $topping . $divider . "Sausage"; 
                                        break;
                                    case "K":
                                        $topping = $topping . $divider . "Bacon"; 
                                        break;
                                    case "B":
                                        $topping = $topping . $divider . "Beef"; 
                                        break;
                                    case "U":
                                        $topping = $topping . $divider . "Chicken"; 
                                        break;
                                    case "M":
                                        $topping = $topping . $divider . "Mushroom"; 
                                        break;
                                    case "O":
                                        $topping = $topping . $divider . "Onion"; 
                                        break;
                                    case "G":
                                        $topping = $topping . $divider . "Green Pepper"; 
                                        break;
                                    case "R":
                                        $topping = $topping . $divider . "Black Olive"; 
                                        break;
                                    case "Z":
                                        $topping = $topping . $divider . "Banana Pepper"; 
                                        break;
                                    case "T":
                                        $topping = $topping . $divider . "Tomato"; 
                                        break;
                                    case "J":
                                        $topping = $topping . $divider . "Jalapeno"; 
                                        break;
                                    case "Q":
                                        $topping = $topping . $divider . "BBQ"; 
                                        break;
                                    case "Y":
                                        $topping = $topping . $divider . "Cheddar Cheese"; 
                                        break;
                                }                                
                            }
                            
                            ($topping == "") ? print("None") : print $topping;                       
                        ?>
                    </p>
                    <p>Action: <?php echo $action; ?>
                </form>
            </p>
        </div>
    </section>
</main>
<?php include 'footer.php'; ?>