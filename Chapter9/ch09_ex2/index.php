<?php
//set default values
$number1 = 78;
$number2 = -105.33;
$number3 = 0.0049;
$message = 'Enter some numbers and click on the Submit button.';

//process
$action = filter_input(INPUT_POST, 'action');
switch ($action) {
    case 'process_data':
        $number1 = filter_input(INPUT_POST, 'number1', FILTER_VALIDATE_INT);
        $number2 = filter_input(INPUT_POST, 'number2', FILTER_VALIDATE_FLOAT);
        $number3 = filter_input(INPUT_POST, 'number3', FILTER_VALIDATE_FLOAT);

        // make sure the user enters three numbers
        // make sure the numbers are valid
        if (is_numeric($number1) && is_numeric($number2) && is_numeric($number3)){
            // get the ceiling and floor for $number2
            $number2_ceil = ceil($number2);
            $number2_floor = floor($number2);
            
            // round $number3 to 3 decimal places
            $number3_rounded = round($number3, 3);
            
            // get the max and min values of all three numbers
            $number_max = max($number1, $number2, $number3);
            $number_min = min($number1, $number2, $number3);
            
            // generate a random integer between 1 and 100
            $number_rand = rand(1, 100);
            
        }
    

        // format the message
        $message = "Number 1: $number1\n" .
            "Number 2: $number2\n" .
            "Number 3: $number3\n" .
            "\n" .
            "Number 2 ceiling: $number2_ceil\n" .
            "Number 2 floor: $number2_floor\n" .
            "Number 3 rounded: $number3_rounded\n" .
            "\n" .
            "Min: $number_min\n" .
            "Max: $number_max\n" .
            "\n" .
            "Random: $number_rand\n";

        break;
}
include 'number_tester.php';
?>