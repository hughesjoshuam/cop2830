<?php 
/* Joshua M. Hughes
 * COP2830
 * 4/13/2017
 * Professor Barrell
 * 
 * index.php file
 * Chapter 9 Murach PHP & MySQl
 */
include 'header.php'; 

?>

<main>      
    <h1>Chapter 9 - In Class Walk Through</h1>
    <!-- Display a list of forms -->
    <h2>Strings and Numbers</h2>
    <section>
        
        <!---How to create strings--->
        <article>
            <h3>How to create strings</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>Assign strings with single quotes</td>
                    <td>
                        $language = 'PHP';
                        $message = 'Welcome to ' . $language;
                        
                        $query = 'SELECT firstName, lastName, FROM Users';
                    </td>
                    <td>
                    <?php 
                        $language = 'PHP';
                        $message = 'Welcome to ' . $language;
                        
                        $query = 'SELECT firstName, lastName, FROM Users';
                        echo 'Language: ' . $language;
                        echo 'Message: ' . $message;
                        echo 'Query: ' . $query;
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Assign strings with double quotes</td>
                    <td>
                        $language = "PHP";
                        $message = "Welcome to $language";
                    </td>
                    <td>
                    <?php 
                        $language = "PHP";
                        $message = "Welcome to $language";
                        echo $message;
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Using braces with variable substitution</td>
                    <td>
                        $count = 12;
                        $item = "flower";
                        $message1 = "You bought $count $items.";
                        $message2 = "You bought $county ${item}s.";
                    </td>
                    <td>
                    <?php 
                        $count = 12;
                        $item = "flower";
                        $items = "";
                        $message1 = "You bought $count $items.";
                        $message2 = "You bought $count ${item}s.";
                        echo $message1;
                        echo $message2;
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Assign a string with heredoc</td>
                    <td>
                        $language = 'PHP';
                        $message = &lt;&lt;&lt;MESSAGE
The heredoc syntax allows you to build multi-line
strings in $language. Inside, it acts like a double-quoted
string and performs variable substitution
MESSAGE;
                        ECHO $message;
                    </td>
                    <td>
                    <?php 
                        $language = 'PHP';
                        $message = <<<MESSAGE
The heredoc syntax allows you to build multi-line
strings in $language. Inside, it acts like a double-quoted
string and performs variable substitution.
MESSAGE;
                        echo $message;
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Assign a string with a nowdoc</td>
                    <td>
                        $message = &lt;&lt;&lt;'MESSAGE'
The nowdoc syntax allows you to build multi-line
strings in PHP. However, no variable interpolation takes place
inside the nowdoc string. This is similar to single quoted strings.
MESSAGE;
                    </td>
                    <td>
                    <?php 
                        $message = <<<'MESSAGE'
The nowdoc syntax allows you to build multi-line
strings in PHP. However, no variable interpolation takes place
inside the nowdoc string. This is similar to single quoted strings.
MESSAGE;
                        echo $message;
                    ?>
                    </td>
                </tr>
            </table>
        </article>     
        
        <!---How to use escape sequences--->
        <article>
            <h3>How to use escape sequences</h3>
            <h4>Escape sequences only used in some strings</h4>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Used In</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>Backslash</td>
                    <td>All strings except nowdocs</td>
                    <td>$dir = 'C:\\xampp\\php';</td>
                    <td>
                    <?php
                        $dir = 'C:\\xampp\\php';
                        echo $dir;
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Single Quote</td>
                    <td>Single-quoted strings</td>
                    <td>$name = 'Mike\'s Music Store';</td>
                    <td>
                    <?php
                        $name = 'Mike\'s Music Store';
                        echo $name;
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Double Quote</td>
                    <td>Double-quoted strings</td>
                    <td>$quote = "He said, \"It costs \$12.\"";</td>
                    <td>
                    <?php
                        $quote = "He said, \"It costs \$12.\"";
                        echo $quote;
                    ?>
                    </td>
                </tr>
            </table>
        </article>     
        
        <!---How to use escape sequences--->
        <article>
            <h3>How to use escape sequences</h3>
            <h4>Escape sequences used in double-quoted strings and heredocs</h4>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Sequence</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>     
                <tr>
                    <td>Dollar Sign</td>
                    <td>\$</td>
                    <td>$quote = "He said, \"It costs \$12.\"";</td>
                    <td>
                    <?php
                        $quote = "He said, \"It costs \$12.\"";
                        echo $quote;
                    ?>
                    </td>
                </tr>     
                <tr>
                    <td>New Line</td>
                    <td>\n</td>
                    <td>$comment1 = "This is a\nmulti-line string.";</td>
                    <td>
                    <?php
                        $comment1 = "This is a \n multi-line string."; // This isn't working as expected.
                        echo $comment1;
                    ?>
                    </td>
                </tr>     
                <tr>
                    <td>Tab</td>
                    <td>\t</td>
                    <td>$comment1 = "This is a\tmulti-line string.";</td>
                    <td>
                    <?php
                        $comment1 = "This is a \t multi-line string.";
                        echo $comment1;
                    ?>
                    </td>
                </tr>     
                <tr>
                    <td>Carriage Return</td>
                    <td>\r</td>
                    <td>$comment1 = "This is a\rmulti-line string.";</td>
                    <td>
                    <?php
                        $comment1 = "This is a \r multi-line string.";
                        echo $comment1;
                    ?>
                    </td>
                </tr>     
                <tr>
                    <td>Form Feed</td>
                    <td>\f</td>
                    <td>$comment1 = "This is a\fmulti-line string.";</td>
                    <td>
                    <?php
                        $comment1 = "This is a \f multi-line string.";
                        echo $comment1;
                    ?>
                    </td>
                </tr>     
                <tr>
                    <td>Vertical Tab</td>
                    <td>\v</td>
                    <td>$comment1 = "This is a\vmulti-line string.";</td>
                    <td>
                    <?php
                        $comment1 = "This is a \v multi-line string.";
                        echo $comment1;
                    ?>
                    </td>
                </tr>     
                <tr>
                    <td>Character with the specified octal value</td>
                    <td>\ooo</td>
                    <td>$comment1 = "This is a \123 multi-line string.";</td>
                    <td>
                    <?php
                        $comment1 = "This is a \123 multi-line string.";
                        echo $comment1;
                    ?>
                    </td>
                </tr>     
                <tr>
                    <td>Character with the specified hexadecimal value</td>
                    <td>\xhh</td>
                    <td>$comment1 = "This is a \x1e multi-line string.";</td>
                    <td>
                    <?php
                        $comment1 = "This is a \x1e multi-line string.";
                        echo $comment1;
                    ?>
                    </td>
                </tr>
            </table>
        </article>         
        
        <!---HTML Entities--->
        <article>
            <h3>HTML Entities</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Function</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>     
                <tr>
                    <td>Returns a string with all special HTML characters converted to the HTML entity. 
                        You can use the $quotes parameter to control how single and double quotes are converted.</td>
                    <td>htmlentities($str, $quotes);</td>
                    <td>
                        $copyright1 = "\xa9 2014";<br />
                        $copyright2 = htmlentities("\xa9 2014");<br />
                    </td>
                    <td>
                        <?php
                            $copyright1 = "\xa9 2014";
                            $copyright2 = htmlentities("\xa9 2014", ENT_QUOTES);
                            echo $copyright1;
                            echo $copyright2;
                        ?>
                    </td>
                </tr>
            </table>
        </article>         
        
        <!---String Length and substring--->
        <article>
            <h3>String Length and substring</h3>
            <table>
                <tr>
                    <th>Function</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>    
                <tr>
                    <td>empty(@str)</td>
                    <td>Returns TRUE if $str is an empty string (""), a NULL value, or "0" (0 as a string).
                    This function also returns TRUE if $str isn't set.</td>
                    <td> if(empty($first_name)) {
                    $message = 'You must enter the first name.';
                    }
                    </td>
                    <td>
                        <?php  
                        if(empty($first_name)) {
                            $message = 'You must enter the first name.';
                        }
                        echo $message;
                        ?>
                    </td>
                </tr>   
                <tr>
                    <td>strlen(@str)</td>
                    <td>Returns the lenght of the string.</td>
                    <td>$name = 'Joshua M. Hughes';
                        strlen($name);</td>
                    <td>
                        <?php  
                        $name = 'Joshua M. Hughes';
                        echo strlen($name);
                        ?>
                    </td>
                </tr>   
                <tr>
                    <td>substr($str, $i, $len)</td>
                    <td>Returns a substring of $str starting at position $i and containgin the number of characters specified by $len.
                    If $len is omitted, the function returns the substring starting at position $i through the end of the string.</td>
                    <td>$name = 'Joshua M. Hughes';
                        $firstname = substr($name, 0, 6);
                        $lastname = substr($name, 10);
                    </td>
                    <td>
                        <?php  
                        $name = 'Joshua M. Hughes';
                        $firstname = substr($name, 0, 6);
                        $lastname = substr($name, 10);
                        echo $firstname;
                        echo $lastname;
                        ?>
                    </td>
                </tr>
            </table>
        </article>         
        
        <!---Search a string--->
        <article>
            <h3>Search a string</h3>
            <table>
                <tr>
                    <th>Function</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>   
                <tr>
                    <td>strpos($str1, $str2, $offset)</td>
                    <td>Searches $str 1 for an occurrence of $str2. If $str2 is found, returns an integer value for the position.
                    If $str2 isn't found, returns FALSE. By default, the search starts at position 0, but you can use $offset to
                    specify the start position.</td>
                    <td>$name = 'Martin Van Buren';
                    $i = strpos($name, ' ', 7);</td>
                    <td>
                        <?php
                        $name = 'Martin Van Buren';
                        $i = strpos($name, ' ', 7);
                        echo $i;
                        ?>
                    </td>
                </tr>  
                <tr>
                    <td>stripos($str1, $str2, $offset)</td>
                    <td>A version of strpos that is case-insensitive.</td>
                    <td>$name = 'Martin Van Buren';
                        $i = stripos($name, 'van');</td>
                    <td>
                        <?php
                        $name = 'Martin Van Buren';
                        $i = stripos($name, 'van');
                        echo $i;
                        ?>
                    </td>
                </tr>  
                <tr>
                    <td>strrpos($str1, $str2, $offset)</td>
                    <td>A version of strpos that searches in reverse, from the end of the string to the start.</td>
                    <td>$name = 'Martin Van Buren';
                        $i = strrpos($name, ' ');</td>
                    <td>
                        <?php
                        $name = 'Martin Van Buren';
                        $i = strrpos($name, ' ');
                        echo $i;
                        ?>
                    </td>
                </tr>  
                <tr>
                    <td>strripos($str1, $str2, $offset)</td>
                    <td>A version of strrpos that is case-insensitive.</td>
                    <td>$name = 'Martin Van Buren';
                        $i = strripos($name, 'A');</td>
                    <td>
                        <?php
                        $name = 'Martin Van Buren';
                        $i = strripos($name, 'A');
                        echo $i;
                        ?>
                    </td>
                </tr>
            </table>
        </article>            
        
        <!---Replace a string--->
        <article>
            <h3>Replace a string</h3>
            <table>
                <tr>
                    <th>Function</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>   
                <tr>
                    <td>str_replace($str1, $new, $str2)</td>
                    <td>Returns a new string with all occurences of $str1 in $str2 replaced with $new. It is case sensitive.</td>
                    <td>$phone = '555.867.53096;
                    $phone = str_replace('.', '-', $phone);</td>
                    <td>
                        <?php
                        $phone = '555.867.5309';
                        $phone = str_replace('.', '-', $phone);
                        echo $phone;
                        ?>
                    </td>
                </tr>   
                <tr>
                    <td>str_ireplace($str1, $new, $str2)</td>
                    <td>A version of str_replace that is case-insensitive.</td>
                    <td>$message = 'Hello Professor';
                        $message = str_ireplace('hello', 'Howdy', $message);</td>
                    <td>
                        <?php
                        $message = 'Hello Professor';
                        $message = str_ireplace('hello', 'Howdy', $message);
                        echo $message;
                        ?>
                    </td>
                </tr>
            </table>
        </article>             
        
        <!---Modify a string--->
        <article>
            <h3>Modify a string</h3>
            <table>
                <tr>
                    <th>Function</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>   
                <tr>
                    <td>ltrim($str)</td>
                    <td>Returns a new string with white space trimmed from the left side of the string.</td>
                    <td>$name = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Joshua M. Hughes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    echo ltrim($name);</td>
                    <td>
                        <?php
                        $name = '   Joshua M. Hughes    ';
                        echo ltrim($name);
                        ?>
                    </td>
                </tr>   
                <tr>
                    <td>rtrim($str)</td>
                    <td>Returns a new string with white space trimmed from the right side of the string.</td>
                    <td>$name = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Joshua M. Hughes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    echo rtrim($name);</td>
                    <td>
                        <?php
                        $name = '   Joshua M. Hughes    ';
                        echo rtrim($name);
                        ?>
                    </td>
                </tr>   
                <tr>
                    <td>trim($str)</td>
                    <td>Returns a new string with white space trimmed from both sides of the string.</td>
                    <td>$name = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Joshua M. Hughes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    echo trim($name);</td>
                    <td>
                        <?php
                        $name = '   Joshua M. Hughes    ';
                        echo trim($name);
                        ?>
                    </td>
                </tr>   
                <tr>
                    <td>str_pad($str, $len, $pad, $type)</td>
                    <td>Returns a new string with $str padded on the right with spaces until it is $len characters long.
                        By default, the pad character is a space, but you can use the optional $pad parameter to specify a different
                        padding character. By default, padding is added to the right, but you can use the optional $type parameter to add
                        padding to the left, right, or both. To do that, you can use these contsnts STR_PAD_RIGHT, STR_PAD_LEFT, STR_PAD_BOTH.</td>
                    <td>$name = 'Joshua M. Hughes';
                    echo str_pad($name, 20, 0, STR_PAD_BOTH);</td>
                    <td>
                        <?php
                        $name = 'Joshua M. Hughes';
                        echo str_pad($name, 20, 0, STR_PAD_BOTH);
                        ?>
                    </td>
                </tr>  
                <tr>
                    <td>lcfirst($str)</td>
                    <td>Returns a new string with the first character converted to lowercase.</td>
                    <td>$name = 'Joshua M. Hughes';
                    echo lcfirst($name);</td>
                    <td>
                        <?php
                        $name = 'Joshua M. Hughes';
                        echo lcfirst($name);
                        ?>
                    </td>
                </tr> 
                <tr>
                    <td>ucfirst($str)</td>
                    <td>Returns a new string with the first character converted to uppercase.</td>
                    <td>$name = 'joshua M. Hughes';
                    echo ucfirst($name);</td>
                    <td>
                        <?php
                        $name = 'joshua M. Hughes';
                        echo ucfirst($name);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>ucwords($str)</td>
                    <td>Returns a new string with the first character converted to uppercase.</td>
                    <td>$name = 'joshua m. hughes';
                    echo ucwords($name);</td>
                    <td>
                        <?php
                        $name = 'joshua m. hughes';
                        echo ucwords($name);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>strtolower($str)</td>
                    <td>Returns a new string with all characters converted to lowercase.</td>
                    <td>$name = 'Joshua M. Hughes';
                    echo strtolower($name);</td>
                    <td>
                        <?php
                        $name = 'Joshua M. Hughes';
                        echo strtolower($name);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>strtoupper($str)</td>
                    <td>Returns a new string with all characters converted to uppercase.</td>
                    <td>$name = 'Joshua M. Hughes';
                    echo strtoupper($name);</td>
                    <td>
                        <?php
                        $name = 'Joshua M. Hughes';
                        echo strtoupper($name);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>strrev($str)</td>
                    <td>Returns a new string with the sequence of characters in reverse.</td>
                    <td>$name = 'Joshua M. Hughes';
                    echo strrev($name);</td>
                    <td>
                        <?php
                        $name = 'Joshua M. Hughes';
                        echo strrev($name);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>str_shuffle($str)</td>
                    <td>Returns a new string with the sequence of characters randomly shuffled.</td>
                    <td>$name = 'Joshua M. Hughes';
                    echo str_shuffle($name);</td>
                    <td>
                        <?php
                        $name = 'Joshua M. Hughes';
                        echo str_shuffle($name);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>str_repeat($str, $i)</td>
                    <td>Returns a new string with $str repeated $i times.</td>
                    <td>$name = 'Joshua M. Hughes';
                    echo str_repeat($name, 3);</td>
                    <td>
                        <?php
                        $name = 'Joshua M. Hughes';
                        echo str_repeat($name, 3);
                        ?>
                    </td>
                </tr>
            </table>
        </article>             
        
        <!---Convert between strings and arrays--->
        <article>
            <h3>Convert between strings and arrays</h3>
            <table>
                <tr>
                    <th>Function</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>   
                <tr>
                    <td>explode($sep, $str)</td>
                    <td>Returns an array of strings that are separated in $str by the string delimeter specified by $sep.
                    This delimeter can be any length. If it is the empty string, it returns FALSE.</td>
                    <td>$names = 'Mike|Ann|Joel|Ray|Josh';
                    $names = explode('|', $names);</td>
                    <td>
                        <?php
                        $names = 'Mike|Ann|Joel|Ray|Josh';
                        $names = explode('|', $names);
                        print_r($names);
                        ?>
                    </td>
                </tr>  
                <tr>
                    <td>implode($sep, $sa)</td>
                    <td>Returns an string that results from joining the elements in the array $sa with the $sep string between them.
                    The $sep parameter is required, but it may be an empty string.</td>
                    <td>$names = implode('|', $names);</td>
                    <td>
                        <?php
                        $names = implode('|', $names);
                        echo $names;
                        ?>
                    </td>
                </tr>
            </table>
        </article>
        
        <!---Functions that compare two strings--->
        <article>
            <h3>Functions that compare two strings</h3>
            <table>
                <tr>
                    <th>Function</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>   
                <tr>
                    <td>strcmp($str1, $str2)</td>
                    <td>Compares two strings and returns and integer value that indicates their sequence: -1 if $str1 come before $str2,
                    1 if $str1 comes after $str2, or 0 if they're the same. This comparison is case-sensitive. As a result, uppercase comes before lowercase.</td>
                    <td>
                        $result1 = strcmp('Anders', 'Zylka');
                        $result2 = strcmp('anders', 'Zylka');
                    </td>
                    <td><?php $result1 = strcmp('Anders', 'Zylka');
                        $result2 = strcmp('anders', 'Zylka');
                        echo $result1 . '<br />';
                        echo $result2 ?></td>
                </tr>
                <tr>
                    <td>strcasecmp($str1, $str2)</td>
                    <td>A version of strcmp that is case-insensitive.</td>
                    <td>$result = strcasecmp('Anders', 'zylka');</td>
                    <td><?php $result = strcasecmp('Anders', 'zylka'); echo $result; ?></td>
                </tr>
                <tr>
                    <td>strnatcmp($str1, $str2)</td>
                    <td>A version of the strcmp function that uses "natural" comparison for numbers within the string.</td>
                    <td>$result = strnatcmp('img6', 'img10');</td>
                    <td><?php $result = strnatcmp('img6', 'img10'); echo $result; ?></td>
                </tr>
                <tr>
                    <td>strnatcasecmp($str1, $str2)</td>
                    <td>A version of the strcmp function that uses a "natural" comparison for numbers and is not case-sensitive.</td>
                    <td>
                        $result = strnatcasecmp($name_1, $name_2);
                        if($result < 0) {
                            echo $name_1 . ' before ' . $name_2;
                        }else if ($result  ==  0){
                            echo $name_1 . ' matches ' . $name_2;
                        } else {
                            echo $name_1 . ' after ' . $name_2;
                        }
                    </td>
                    <td><?php
                        $name_1 = 'Anders01';
                        $name_2 = 'zylka45';
                        $result = strnatcasecmp($name_1, $name_2);
                        if($result < 0) {
                            echo $name_1 . ' before ' . $name_2;
                        }else if ($result  ==  0){
                            echo $name_1 . ' matches ' . $name_2;
                        } else {
                            echo $name_1 . ' after ' . $name_2;
                        }
                    ?></td>
                </tr>
            </table>
        </article>
                
        <!---Working with Numbers--->
        <article>
            <h3>Working with Numbers</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>Assign a decimal value (base10)</td>
                    <td>
                        $number_1 = 42;<br />
                        $number_2 = +72;<br />
                        $number_3 = -13;<br />
                        $number_4 = -(-39);<br />
                        $number_5 = --39;<br />
                    </td>
                    <td><?php
                        $number_1 = 42;
                        echo $number_1 . '<br />';
                        $number_2 = +72;
                        echo $number_2 . '<br />';
                        $number_3 = -13;
                        echo $number_3 . '<br />';
                        $number_4 = -(-39);
                        echo $number_4 . '<br />';
                       // $number_5 = --39;
                       // echo $number_5 . '<br />';
                       // Results in error.
                    ?></td>
                </tr>
                <tr>
                    <td>Maximum and minimum integer values (base10)</td>
                    <td>$max_int = PHP_INT_MAX;
                        $min_int = (-1 * PHP_INT_MAX) - 1;</td>
                    <td><?php
                        $max_int = PHP_INT_MAX;
                        $min_int = (-1 * PHP_INT_MAX) - 1;
                        echo $max_int . '<br />';
                        echo $min_int . '<br />';
                    ?></td>
                </tr>
                <tr>
                    <td>Assign and octal value (base8)</td>
                    <td>$octal_1 = 0251;<br />
                        $octal_2 = -0262;
                    </td>
                    <td><?php
                        $octal_1 = 0251;
                        echo $octal_1 . '<br />';
                        $octal_2 = -0262;
                        echo $octal_2;
                    ?></td>
                </tr>
                <tr>
                    <td>Assign a hexadecimal value (base16)</td>
                    <td>$hex_1 = 0x5F;<br />
                    $hex_2 = 0x4a3b;</td>
                    <td><?php 
                        $hex_1 = 0x5F;
                        echo $hex_1 . '<br />';
                        $hex_2 = 0x4a3b;
                        echo $hex_2;
                    ?></td>
                </tr>
                <tr>
                    <td>Assign a floating-point value - normal notation</td>
                    <td>
                        $float_1 = 3.5;<br />
                        $float_2 = -6.0;<br />
                        $float_3 = .125;<br />
                        $float_4 = 1.;<br />
                    </td>
                    <td><?php
                        $float_1 = 3.5;
                        echo $float_1 . '<br />';
                        $float_2 = -6.0;
                        echo $float_2 .'<br />';
                        $float_3 = .125;
                        echo $float_3 . '<br />';
                        $float_4 = 1.;
                        echo $float_4 . '<br />';
                    ?></td>
                </tr>
                <tr>
                    <td>Assign a floating-point value - exponential notation</td>
                    <td>
                        $exp_1 = 9.451e15;<br />
                        $exp_2 = 6.022e+23;<br />
                        $exp_3 = 1.602e-19;<br />
                        $exp_4 = 9.806e0;<br />
                        $exp_5 = -1.759e11;<br />
                        $exp_6 = 3e9;<br />
                    </td>
                    <td><?php
                        $exp_1 = 9.451e15;
                        echo $exp_1 . '<br />';
                        $exp_2 = 6.022e+23;
                        echo $exp_2 . '<br />';
                        $exp_3 = 1.602e-19;
                        echo $exp_3 . '<br />';
                        $exp_4 = 9.806e0;
                        echo $exp_4 . '<br />';
                        $exp_5 = -1.759e11;
                        echo $exp_5 . '<br />';
                        $exp_6 = 3e9;
                        echo $exp_6 . '<br />';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Working with infinity</td>
                    <td>
                        $result = 1e200 * 1e200;
                        if (is_infinite($result)) {
                            echo('Result was out of range.');
                        } else {
                            echo('Result is ' . $result);
                        }
                    </td>
                    <td><?php
                        $result = 1e200 * 1e200;
                        if (is_infinite($result)) {
                            echo('Result was out of range.');
                        } else {
                            echo('Result is ' . $result);
                        }  
                    ?>
                    </td>
                </tr>
            </table>
        </article>
                
        <!---Common mathematical functions--->
        <article>
            <h3>Common mathematical functions</h3>
            <table>
                <tr>
                    <th>Function</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>   
                <tr>
                    <td>abs($value)</td>
                    <td>Returns the absolute value of a number.</td>
                    <td>abs(-6);</td>
                    <td><?php echo abs(-6); ?></td>
                </tr>  
                <tr>
                    <td>ceil($value)</td>
                    <td>Returns the value rounded up to the next highest hole number.</td>
                    <td>ceil(25.56);</td>
                    <td><?php echo ceil(25.56); ?></td>
                </tr> 
                <tr>
                    <td>floor($value)</td>
                    <td>Returns the value rounded down to the next lowest hole number.</td>
                    <td>floor(25.56);</td>
                    <td><?php echo floor(25.56); ?></td>
                </tr> 
                <tr>
                    <td>max($value1, $value2, $value3)</td>
                    <td>Returns the value of the largest number provided.</td>
                    <td>max(25.56, 32.12, 14.32);</td>
                    <td><?php echo max(25.56, 32.12, 14.32); ?></td>
                </tr>
                <tr>
                    <td>min($value1, $value2, $value3)</td>
                    <td>Returns the value of the smallest number provided.</td>
                    <td>min(25.56, 32.12, 14.32);</td>
                    <td><?php echo min(25.56, 32.12, 14.32); ?></td>
                </tr>
                <tr>
                    <td>pi()</td>
                    <td>Returns the value of pi.</td>
                    <td>pi();</td>
                    <td><?php echo pi(); ?></td>
                </tr>
                <tr>
                    <td>pow($base, $exp)</td>
                    <td>Returns the value of the calculation of $base to the power of $exp. Both parameters may be floating-point numbers.</td>
                    <td>pow(26.32, 5);</td>
                    <td><?php echo pow(26.32, 5); ?></td>
                </tr>
                <tr>
                    <td>round($value, $precision)</td>
                    <td>Returns the value that results from rounding $value to the number of decimal points specified by $precision. 
                    If $precision is omitted, its default is 0. If $precision is negative, this method rounds to a whole number. For example,
                    -1 rounds to the nearest ten and -2 rounds to the nearest hundred.</td>
                    <td>round(3.1415926535898, 5);</td>
                    <td><?php echo round(3.1415926535898, 5); ?></td>
                </tr>
                <tr>
                    <td>sqrt($value)</td>
                    <td>Returns the square root of $value.</td>
                    <td>sqrt(100);</td>
                    <td><?php echo sqrt(100); ?></td>
                </tr>
            </table>
        </article>
        
        <!---Random Number Functions--->
        <article>
            <h3>Random Number Functions</h3>
            <table>
                <tr>
                    <th>Function</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>   
                <tr>
                    <td>getrandmax()</td>
                    <td>Returns the largest integer the rand function can return. This may be a relatively small number (about 32000) on some systems.</td>
                    <td>getrandmax()</td>
                    <td><?php echo getrandmax(); ?></td>
                </tr>  
                <tr>
                    <td>rand()</td>
                    <td>Returns a random integer between 0 and getrandmax().</td>
                    <td>rand()</td>
                    <td><?php echo rand(); ?></td>
                </tr>
                <tr>
                    <td>rand($lo, $hi)</td>
                    <td>Returns a random integer between $lo and $hi.</td>
                    <td>rand(0, 10)</td>
                    <td><?php echo rand(0, 10); ?></td>
                </tr>
                <tr>
                    <td>mt_getrandmax()</td>
                    <td>Returns the largest integer the mt_rand function can return. This number is typically PHP_INT_MAX.</td>
                    <td>mt_getrandmax()</td>
                    <td><?php echo mt_getrandmax(); ?></td>
                </tr>   
                <tr>
                    <td>mt_rand()</td>
                    <td>Returns a random integer between 0 and mt_getrandmax(). This function uses the Mersenne Twister algorithm which is faster and "more random" than the algorithm
                    used by the rand function.</td>
                    <td>mt_rand()</td>
                    <td><?php echo mt_rand(); ?></td>
                </tr>
                <tr>
                    <td>mt_rand($lo, $hi)</td>
                    <td>Returns a random integer between $lo and $hi using the Mersenne Twister algorithm.</td>
                    <td>mt_rand(0, 10)</td>
                    <td><?php echo mt_rand(0, 10); ?></td>
                </tr>
            </table>
        </article>
        
        <!---The sprintf Function--->
        <article>
            <h3>The sprintf Function</h3>
            <table>
                <tr>
                    <th>Function</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>  
                <tr>
                    <td>sprintf($format, $val1, $val2)</td>
                    <td>Returns a string that contains one or more values that are formatted as specified by the $format parameter.</td>
                    <td>$message = sprintf('The book about %s has %d pages.', 'PHP', 800);</td>
                    <td><?php $message = sprintf('The book about %s has %d pages.', 'PHP', 800); echo $message; ?></td>
                </tr>
            </table>
        </article>
        
    </section>
</main>
<?php include 'footer.php'; ?>