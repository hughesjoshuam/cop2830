<?php
//set default values
$name = '';
$email = '';
$phone = '';
$message = 'Enter some data and click on the Submit button.';

//process
$action = filter_input(INPUT_POST, 'action');

switch ($action) {
    case 'process_data':
        $name = filter_input(INPUT_POST, 'name');
        $email = filter_input(INPUT_POST, 'email');
        $phone = filter_input(INPUT_POST, 'phone');

        /*************************************************
         * validate and process the name
         ************************************************/
        // 1. make sure the user enters a name
        if ($name != ''){
            // 2. display the name with only the first letter capitalized
            $firstname = ucfirst(trim(substr(strtolower(trim($name)), 0, strpos($name, ' '))));
            $lastname = ucfirst(trim(substr(strtolower(trim($name)), strpos($name, ' '))));
        } else {
            $message = "You must enter a name.";
            break;
        }
        
        /*************************************************
         * validate and process the email address
         ************************************************/
        // 1. make sure the user enters an email
        if ($email != ''){
            // 2. make sure the email address has at least one @ sign and one dot character    
            if (!strpos($email, "@") > 0 || (abs(strlen(trim($email)) - strrpos(trim($email), ".")-1) > 3 || abs(strlen(trim($email)) - strrpos(trim($email), ".")-1) < 1)){
                $message = "The email address is not properly formatted.";
                break;
            }
        } else {
            $message = "You must enter an email address.";
            break;
        }
        

        /*************************************************
         * validate and process the phone number
         ************************************************/
        // 1. make sure the user enters at least seven digits, not including formatting characters
        if ($phone != ''){
            //Create an array and insert numeric characters into array
            $phone_numbers = array();
            for( $i = 0; $i < strlen($phone); $i++){
                $phone_character = substr($phone, $i, 1);
                if (is_numeric($phone_character) && $phone_character != "+" && $phone_character != "e" && $phone_character != "."){
                    $phone_numbers[count($phone_numbers)] = $phone_character;
                }
            }
            
            //If the number of numeric characters is equal to a known format 
            if (count($phone_numbers) == 7 || count($phone_numbers) == 10 || (count($phone_numbers) >= 11 && count($phone_numbers) <= 13)){
                $phone = '';
                switch (count($phone_numbers)){
                    case 7:
                        for($i = 0; $i < 7; $i++){
                            if( $i != 3){
                               $phone = $phone . $phone_numbers[$i];
                            } else {
                               $phone = $phone . "-" . $phone_numbers[$i];
                            }
                        }
                        break;
                    case 10:
                        for($i = 0; $i < 10; $i++){
                            if( $i != 3 && $i != 6){
                               $phone = $phone . $phone_numbers[$i];
                            } else {
                               $phone = $phone . "-" . $phone_numbers[$i];
                            }
                        }
                        break;
                    default:
                        if (count($phone_numbers) >= 11 && count($phone_numbers) <= 13){
                            for($i = 0; $i < count($phone_numbers); $i++){
                                if(count($phone_numbers) == 11){
                                    if( $i != 1 && $i != 4 && $i != 7){
                                        $phone = $phone . $phone_numbers[$i];
                                    } else {
                                        $phone = $phone . "-" . $phone_numbers[$i];
                                    }
                                } else if(count($phone_numbers) == 12){
                                    if( $i != 2 && $i != 5 && $i != 8){
                                        $phone = $phone . $phone_numbers[$i];
                                    } else {
                                        $phone = $phone . "-" . $phone_numbers[$i];
                                    }
                                } else if(count($phone_numbers)==13){
                                    if( $i != 3 && $i != 6 && $i != 9){
                                        $phone = $phone . $phone_numbers[$i];
                                    } else {
                                        $phone = $phone . "-" . $phone_numbers[$i];
                                    }
                                }                                
                            }
                        } else {
                            $message = "You must enter a valid phone number.";
                            break;
                        }
                        break;
                }
            } else {
                $message = "You must enter a valid phone number.";
                break;
            }
        } else {
            $message = "You must enter an phone number.";
            break;
        }
        // 2. format the phone number like this 123-4567 or this 123-456-7890
        
        /*************************************************
         * Display the validation message
         ************************************************/
        $message = "Hello " . $firstname . ",\n" .
                   "\n" .
                   "Thank you for entering this data: \n" .
                   "\n" .
                   "Name: " . $firstname . " " . $lastname . "\n" .
                   "Email: " . $email . "\n" .
                   "Phone: " . $phone . "\n";

        break;
}
include 'string_tester.php';
?>