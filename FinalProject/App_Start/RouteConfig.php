<?php

/* 
 * Joshua M. Hughes
 * COP28/30
 * Professor Barrell
 * April 20, 2017
 * 
 * Final Project: RouteConfig.php - Routing logic for application.
 */

function RouteControl(){
    
    isset($_SERVER['REQUEST_URI']) ? $script_name = $_SERVER['REQUEST_URI'] : $script_name = null;
    isset($_SERVER['REQUEST_METHOD']) ? $request_method = $_SERVER['REQUEST_METHOD'] : $request_method = null;
    
    /* 
     * Determine Route Values based upon URL String.
     * If value is empty, set default route value
     */
    $RouteMap = explode('/', $script_name, 4);
    isset($RouteMap[1]) && $RouteMap[1] <> '' && substr($RouteMap[1], 0, 1) <> '?' ? $controller = $RouteMap[1] : $controller = 'Home';
    isset($RouteMap[2]) && $RouteMap[2] <> '' && substr($RouteMap[2], 0, 1) <> '?' ? $view = $RouteMap[2] : $view = 'Index';
    isset($RouteMap[3]) && $RouteMap[3] <> '' && substr($RouteMap[3], 0, 1) <> '?' ? $id = $RouteMap[3] : $id = null;
    
    /*
     * Drop initial values and recreate RouteMap Array.
     * Note: Original array includes other values not used in route determination.
     */
    $RouteMap = array(
            'controller' => $controller,
            'view' => $view,
            'id' => $id
        );
    
    return $RouteMap;
    
}

?>