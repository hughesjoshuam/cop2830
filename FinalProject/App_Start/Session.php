<?php

/* 
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * April 23, 2017
 * 
 * Final Project: Session.php - Class for managing Session State
 */

/**
 * Initialize();
 * Destroy();
 */
class Session {
    public static function Initialize(){
        
        /*
         * Initialize Application Session
         * Note: Used in this application similiar to ViewBag in ASP.Net 
         * PHP uses SuperGlobals to cover all scopes
         * http://php.net/manual/en/language.variables.superglobals.php
         */

        session_start();

        /*
         * The code below would implement application security and authentication.
         * I have provided commented examples which will simulate different security and
         * authentication scenarios. The resulting action is to exit processing. The exit;
         * statement can be replaced with a redirect to the login screen. A login screen
         * is not provided.
         */

        // Simulate Active Session with Valid AUTH_TOKEN [This is default for the application to work.]
        $_SESSION['AUTH_TOKEN'] = session_id();

        // Simulate Active Session with invalid AUTH_TOKEN
        //$_SESSION['AUTH_TOKEN'] = false;

        // Simulate no active session and no valid AUTH_TOKEN. Also used to logout current logged in user.
        //session_destroy();

        // Check for Active Session with Login Token to confirm logged in user
        if(session_status() != 2 || !isset($_SESSION['AUTH_TOKEN'])){ echo 'User not logged in. Redirect to Login Screen.'; exit; }

        // Validate login session by confirming AUTH_TOKEN and SESSION_ID match
        if(session_id() <> $_SESSION['AUTH_TOKEN']){ echo 'User login not validated. Security breach detected! Redirect to Login Screen.'; exit; }
        
    }
    public static function Destroy(){
        
        session_destroy();
        
    }
}
    

