<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: OrderManager - Order Manager Controller
 *
 */

/**
 * Index();
 * OrderDetails();
 * AddOrder();
 * EditOrder();
 * DeleteOrder();
 */
class OrderManager {
    
    public static function Index(){
        $Model = new Orders();
        $model = $Model->Select();
        
        $_SESSION['Page Title'] = 'Order Manager';
        
        return $model;
    }
    
    public static function OrderDetails($orderID){
        $Model = new Orders();
        $model = $Model->Select($orderID);
        
        $_SESSION['Page Title'] = 'Order Details';
        
        return $model;
    }
    
    public static function AddOrder(){
        global $db;
        
        // Check for POST
        count($_POST) > 0 ? $post = true : $post = false;
        // Add Order
        if($post){
            $Model = new Orders();
            $model = $Model->Insert(filter_input(INPUT_POST, 'customerID'), date("Y-m-d H:i:s", strtotime((filter_input(INPUT_POST, 'orderDate')))), 
                    filter_input(INPUT_POST, 'orderDescription'));
            $orderID = $db->lastInsertID();
            
            $aryProductID = $_POST['productID'];
            $aryProductQTY = $_POST['productQty'];
            for($i = 0; $i < count($aryProductID); $i++){
                if($aryProductQTY[$i] != ''){
                    $Model = new Order_Product_Details();
                    $model = $Model->Insert($orderID, $aryProductID[$i], $aryProductQTY[$i]);
                }
            }
            
            // Redirect Back To Index if Successfull
            $model > 0 ? header('Location: \OrderManager\Index') : false;
        }
        
        // Else Add Order Form
        $_SESSION['Page Title'] = 'Add Order';
    }
    
    public static function EditOrder($orderID){
        global $db;
        // Check for POST
        count($_POST) > 0 ? $post = true : $post = false;
        
        if($post){
            $Model = new Orders();
            $model = $Model->Update($orderID, filter_input(INPUT_POST, 'customerID'), date("Y-m-d H:i:s", strtotime((filter_input(INPUT_POST, 'orderDate')))), 
                    filter_input(INPUT_POST, 'orderDescription'));
            
            $aryProductID = $_POST['productID'];
            $aryProductQTY = $_POST['productQty'];
            for($i = 0; $i < count($aryProductID); $i++){
                if($aryProductQTY[$i] != ''){
                    $Model = new Order_Product_Details();
                    $model = $Model->Update($orderID, $aryProductID[$i], $aryProductQTY[$i]);
                }
            }
            
            // Redirect Back To Index if Successfull
            $model ? header('Location: \OrderManager\Index') : false;
        } else {
            $Model = new Orders();
            $model = $Model->Select($orderID);
        }
        
        // Else Add Order Form
        $_SESSION['Page Title'] = 'Add Order';
        
        return $model;
    }
    
    public static function DeleteOrder($orderID){
        global $db;
        // Check for POST
        count($_POST) > 0 ? $post = true : $post = false;
        
        if($post){
            $Model = new Orders();
            $model = $Model->Delete($orderID);
            
            // Redirect Back To Index if Successfull
            $model ? header('Location: \OrderManager\Index') : false;
        } else {
            $Model = new Orders();
            $model = $Model->Select($orderID);
        }
        
        // Else Add Order Form
        $_SESSION['Page Title'] = 'Delete Order';        
        
        return $model;
    }    
}
