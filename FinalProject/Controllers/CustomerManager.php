<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: CustomerManager - #Description#
 *
 */

/**
 * Index();
 * AddCustomer();
 * EditCustomer();
 * DeleteCustomer();
 */
class CustomerManager {
    
    public static function Index(){
        $Model = new Customers();
        $model = $Model->Select();
        
        $_SESSION['Page Title'] = "Customer Manager";
        
        return $model;
    }
    
    public static function AddCustomer(){
        
        // Determine Post and Retrieve Value
        count($_POST) > 0 ? $post = true : $post = false;
            
        if($post){
            $Model = new Customers();
            $model = $Model->Insert(filter_input(INPUT_POST, 'customerName'), filter_input(INPUT_POST, 'customerStreetAddress'), 
                    filter_input(INPUT_POST, 'customerCity'), filter_input(INPUT_POST, 'customerState'), 
                    filter_input(INPUT_POST, 'customerPostalCode'));
            
            // Redirect Back To Index if Successfull
            $model ? header('Location: \CustomerManager\Index') : false; 
        }
        
        $_SESSION['Page Title'] = "Add Customer";
        
    }
    
    public static function EditCustomer($customerID){
        
        // Determine Post and Retrieve Value
        count($_POST) > 0 ? $post = true : $post = false;
        
        if($post){
            $Model = new Customers();
            $model = $Model->Update($customerID, filter_input(INPUT_POST, 'customerName'), filter_input(INPUT_POST, 'customerStreetAddress'), 
                    filter_input(INPUT_POST, 'customerCity'), filter_input(INPUT_POST, 'customerState'), 
                    filter_input(INPUT_POST, 'customerPostalCode'));
            
            // Redirect Back To Index if Successfull
            $model ? header('Location: \CustomerManager\Index') : false; 
        } else {
            $Model = new Customers();
            $model = $Model->Select($customerID);
        }
        
        $_SESSION['Page Title'] = "Edit Customer";
        
        return $model; 
    }
    
    public static function DeleteCustomer($customerID){
        
        // Determine Post and Retrieve Value
        count($_POST) > 0 ? $post = true : $post = false;
        
        if($post){
            $Model = new Customers();
            $model = $Model->Delete($customerID);
            
            // Redirect Back To Index if Successfull
            $model ? header('Location: \CustomerManager\Index') : false;             
        } else {
            $Model = new Customers();
            $model = $Model->Select($customerID);            
        }
        
        $_SESSION['Page Title'] = "Delete Customer";
        
        return $model; 
    }
}
