<?php

/* 
 * Joshua M. Hughes
 * COP28/30
 * Professor Barrell
 * April 20, 2017
 * 
 * Final Project: Home.php - Home Controller
 * 
 */

/**
 * Methods
 * Index();
 * ProductDetails(int $ID);
 *
 */
class Home
{
    
    public static function Index($categoryID = null){
        
        if($categoryID == null){
            /*
            * This line determines the model to be use used.
            */
            $Model = new Products();
            /*
             * This line calls the method within in the model.
             */
            $model = $Model->Select();
        } else {
            $Model = new Products();
            $model = $Model->Select(null, $categoryID);
        }
                
        $_SESSION['Page Title'] = 'Home';
        
        return $model;
    }
    
    public static function ProductDetails($productID){
        $Model = new Products();
        $model = $Model->Select($productID);
        
        $_SESSION['Page Title'] = 'Product Details';
        
        return $model;
    }
    
    public static function About(){
        
        $_SESSION['Page Title'] = 'About Us';
        
        
    }
    
    public static function Repair(){
        
        $_SESSION['Page Title'] = 'Instrument Repair';
        
    }
    
}

?>