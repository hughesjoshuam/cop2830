<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: ProductManager - Product Manager Controller
 *
 */

/**
 * Index();
 * AddProduct();
 * EditProduct();
 * DeleteProduct();
 */
class ProductManager {
    
    public static function Index($categoryID = null){
        if($categoryID == null){
            $Model = new Products();
            $model = $Model->Select();
        } else {
            $Model = new Products();
            $model = $Model->Select(null, $categoryID);
        }
        
        $_SESSION['Page Title'] = 'Product Manager';
        
        return $model;
    }
        
    public static function AddProduct(){
        // Determine if POST
        count($_POST) > 0 ? $post = true : $post = false;
        
        // Add product and redirect
        if($post){
            $Model = new Products();
            $model = $Model->Insert(filter_input(INPUT_POST, 'categoryID'), strtoupper(filter_input(INPUT_POST, 'productCode')), 
                    filter_input(INPUT_POST, 'productName'), filter_input(INPUT_POST, 'listPrice'));
            
            // Redirect Back To Index if Successfull
            $model > 0 ? header('Location: \ProductManager\Index') : false;
        }
        
        // Else load add prodcut form
        $_SESSION['Page Title'] = 'Add Product';
               
    }
    
    public static function EditProduct($productID){
        // Determine if POST
        count($_POST) > 0 ? $post = true : $post = false;
        
        // Edit product and redirect
        if($post){
            $Model = new Products();
            $model = $Model->Update(filter_input(INPUT_POST, 'productID'), filter_input(INPUT_POST, 'categoryID'), 
                    strtoupper(filter_input(INPUT_POST, 'productCode')), filter_input(INPUT_POST, 'productName'), filter_input(INPUT_POST, 'listPrice'));
            
            // Redirect Back To Index if Successfull
            $model ? header('Location: \ProductManager\Index') : false;
        } else {
            $Model = new Products();
            $model = $Model->Select($productID);            
        }
        
        // Else load add prodcut form
        $_SESSION['Page Title'] = 'Edit Product';
        
        return $model;
    }
    
    public static function DeleteProduct($productID){
        // Determine if POST
        count($_POST) > 0 ? $post = true : $post = false;
        
        if($post){
            $Model = new Products();
            $model = $Model->Delete($productID);
            
            // Redirect Back To Index if Successfull
            $model ? header('Location: \ProductManager\Index') : false;
        } else {
            $Model = new Products();
            $model = $Model->Select($productID);
        }
        
        // Else load add prodcut form
        $_SESSION['Page Title'] = 'Delete Product';
        
        return $model;
    }
}
