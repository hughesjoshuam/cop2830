<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: CategoryManager - Category Manager Controller
 *
 */

/**
 * Index();
 * AddCategory();
 * EditCategory();
 * DelteCategory();
 */
class CategoryManager {
    
    public static function Index(){
        $Model = new Categories();
        $model = $Model->Select();
                
        $_SESSION['Page Title'] = 'Category Manager';
        
        return $model;
    }
    
    public static function AddCategory(){
       
        // Determine Post and Retrieve Value
        count($_POST) > 0 ? $categoryName = filter_input(INPUT_POST, 'categoryName') : $categoryName = null;
        
        //If POST Add Category and Redirect
        if ($categoryName != null ){
            $Model = new Categories();
            $model = $Model->Insert($categoryName);
            
            // Redirect Back To Index if Successfull
            $model > 0 ? header('Location: \CategoryManager\Index') : false;
        } 
        
        // Else Load Add Category View
        $_SESSION['Page Title'] = 'Add Category';
        
    }
    
    public static function EditCategory($categoryID){
        
        // Determine Post and Retrieve Value
        count($_POST) > 0 ? $categoryName = filter_input(INPUT_POST, 'categoryName') : $categoryName = null;
        
        // Retrieve Category Model based upon categoryID
        if($categoryName == null){
            $Model = new Categories();
            $model = $Model->Select($categoryID);
        } else {
            //If POST Edit Category and Redirect
            $Model = new Categories();
            $model = $Model->Update($categoryID, $categoryName);
        
            // Redirect Back To Index if Successfull
            $model ? header('Location: \CategoryManager\Index') : false;
        }
        
        // Else Load Edit Category View
        $_SESSION['Page Title'] = 'Edit Category';
        
        return $model;
        
    }
    
    public static function DeleteCategory($categoryID){
        
        // Determine Post and Retrieve Value
        count($_POST) > 0 ? $confirm = true : $confirm = false;
        if($confirm == false){
            $Model = new Categories();
            $model = $Model->Select($categoryID);
        } else {
            $Model = new Categories();
            $model = $Model->Delete($categoryID);
            
            // Redirect Back To Index if Successfull
            $model ? header('Location: \CategoryManager\Index') : false;
        }
                
        $_SESSION['Page Title'] = 'Delete Category';
        
        return $model;
    }
    
}
