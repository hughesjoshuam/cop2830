<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: Orders - Orders Model
 *
 */

/**
 *  PK orderID INT(11)
 *  FK customerID INT(11)
 *  orderDate DATETIME
 *  orderDescription VARCHAR(255)
 */

class Orders {

    public function Select($orderID = null, $customerID = null, $orderBy = 'orderID'){
        global $db;
        $query = 'SELECT * FROM orders '
                . 'INNER JOIN customers ON orders.customerID = customers.customerID '
                . 'WHERE 1 = 1 ';
        if($orderID != null){ $query = $query . 'AND orderID = :orderID '; }
        if($customerID != null){ $query = $query . 'AND customerID = :customerID '; }
        $query = $query . 'ORDER BY :orderBy ';
        
        $statement = $db->prepare($query);
        if($orderID != null){ $statement->bindValue(":orderID", $orderID); }
        if($customerID != null){ $statement->bindValue(":customerID", $customerID); }
        $statement->bindValue(":orderBy", $orderBy);
	$statement->execute();
	$orders = $statement->fetchAll();
	$statement->closeCursor();
	
        return $orders;
    }
    
    public function Insert($customerID, $orderDate, $orderDescription){
        global $db;
        $query = 'INSERT INTO orders (customerID, orderDate, orderDescription) '
                . 'VALUES (:customerID, :orderDate, :orderDescription)';
	
	$statement = $db->prepare($query);
        $statement->bindValue(":customerID", $customerID);
        $statement->bindValue(":orderDate", $orderDate);
        $statement->bindValue(":orderDescription", $orderDescription);
	$orders = $statement->execute();
	$statement->closeCursor();
        
        return $orders;
    }
    
    public function Update($orderID, $customerID, $orderDate, $orderDescription){
        global $db;
        $query = 'UPDATE orders '
                . 'SET customerID = :customerID, orderDate = :orderDate, orderDescription = :orderDescription '
                . 'WHERE orderID = :orderID ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":customerID", $customerID);
        $statement->bindValue(":orderDate", $orderDate);
        $statement->bindValue(":orderDescription", $orderDescription);
        $statement->bindValue(":orderID", $orderID);
	$orders = $statement->execute();
	$statement->closeCursor();
        
        return $orders;
    }
    
    public function Delete($orderID){
        global $db;
        $query = 'DELETE FROM orders WHERE orderID = :orderID ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":orderID", $orderID);
	$orders = $statement->execute();
	$statement->closeCursor();
        
        return $orders;
    }
}
