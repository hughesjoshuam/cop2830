<?php
/* 
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * April 20, 2017
 * 
 * Final Project: Database.php - Establish Database Context using GLOBALS.
 * 
 * Note: Do Not Modify this file unless modifying database context. Individual
 *      settings are configured in App_Start/AppConfig.php
 */
    $dsn = sprintf("mysql:host=%s;dbname=%s",
            $GLOBALS['DATABASE_SERVER'],
            $GLOBALS['DEFAULT_SCHEMA']);

    try {
        $db = new PDO($dsn, $GLOBALS['DATABASE_USERNAME'], $GLOBALS['DATABASE_PASSWORD']);
    } catch (PDOException $e) {
        $error_message = $e->getMessage();
		echo $error_message;
        exit();
    }
?>