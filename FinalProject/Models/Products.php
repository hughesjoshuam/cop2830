<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * April 22, 2017
 * 
 * Final Project: Products.php - Products Model
 */

/**
 * PK productID INT(11)
 * FK categoryID INT(11)
 * productCode VARCHAR(10)
 * productName VARCHAR(255)
 * listPrice DECIMAL(10,2)
 */
class Products {
    public function Select($productID = null, $categoryID = null, $orderBy = 'productID'){
        global $db;
        $query = 'SELECT * FROM products '
                . ' INNER JOIN categories ON products.categoryID = categories.categoryID '
                . ' WHERE 1 = 1 ';
        if($productID != null){ $query = $query . 'AND products.productID = :productID '; }
        if($categoryID != null){ $query = $query . 'AND categories.categoryID = :categoryID '; }
        $query = $query . ' ORDER BY :orderBy ';
        
	$statement = $db->prepare($query);
        if($productID != null){ $statement->bindValue(":productID", $productID); }
        if($categoryID != null){ $statement->bindValue(":categoryID", $categoryID); }
        $statement->bindValue(":orderBy", $orderBy);
	$statement->execute();
	$products = $statement->fetchAll();
	$statement->closeCursor();
        
	return $products;
    }
    
    public function Insert($categoryID, $productCode, $productName, $listPrice){
        global $db;
	$query = 'INSERT INTO products (categoryID, productCode, productName, listPrice) '
                . 'VALUES (:categoryID, :productCode, :productName, :listPrice) ';
	
	$statement = $db->prepare($query);
        $statement->bindValue(":categoryID", $categoryID);
        $statement->bindValue(":productCode", $productCode);
        $statement->bindValue(":productName", $productName);
        $statement->bindValue(":listPrice", $listPrice);
	$products = $statement->execute();
	$statement->closeCursor();
        
        return $products;
    }
    
    public function Update($productID, $categoryID, $productCode, $productName, $listPrice){
        global $db;
        $query = 'UPDATE products '
                . 'SET categoryID = :categoryID, productCode = :productCode, productName = :productName, listPrice = :listPrice '
                . 'WHERE productID = :productID ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":productID", $productID);
        $statement->bindValue(":categoryID", $categoryID);
        $statement->bindValue(":productCode", $productCode);
        $statement->bindValue(":productName", $productName);
        $statement->bindValue(":listPrice", $listPrice);
	$products = $statement->execute();
	$statement->closeCursor();
        
        return $products;
    }
    
    public function Delete($productID){
        global $db;
        $query = 'DELETE FROM products WHERE productID = :productID ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":productID", $productID);
	$products = $statement->execute();
	$statement->closeCursor();
        
        return $products;
    }
}
