<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: Order_Product_Details - Order_Product_Details Model
 *
 */

/**
 *  FK orderID INT(11)
 *  FK productID INT(11)
 *  quantity INT
 */
class Order_Product_Details {
    
    public function Select($orderID, $productID = null, $orderBy = 'orderID'){
        global $db;
        $query = 'SELECT * FROM order_product_details '
                . 'INNER JOIN products ON order_product_details.productID = products.productID '
                . 'INNER JOIN categories ON products.categoryID = categories.categoryID '
                . 'WHERE orderID = :orderID ';
        if($productID != null){ $query = $query . 'AND productID = :productID '; }
        $query = $query . 'ORDER BY :orderBy ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":orderID", $orderID);
        if($productID != null){ $statement->bindValue(":productID", $productID); }
        $statement->bindValue(":orderBy", $orderBy);
	$statement->execute();
	$order_product_details = $statement->fetchAll();
	$statement->closeCursor();
	
        return $order_product_details;
    }
    
    public function Insert($orderID, $productID, $quantity){
        global $db;
        $query = 'INSERT INTO order_product_details (orderID, productID, quantity) '
                . 'VALUES (:orderID, :productID, :quantity)';
	
	$statement = $db->prepare($query);
        $statement->bindValue(":orderID", $orderID);
        $statement->bindValue(":productID", $productID);
        $statement->bindValue(":quantity", $quantity);
	$order_product_details = $statement->execute();
	$statement->closeCursor();
        
        return $order_product_details;
    }
    
    public function Update($orderID, $productID, $quantity){
        global $db;
        $query = 'UPDATE order_product_details '
                . 'SET quantity = :quantity '
                . 'WHERE orderID = :orderID '
                . 'AND productID = :productID ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":quantity", $quantity);
        $statement->bindValue(":orderID", $orderID);
        $statement->bindValue(":productID", $productID);
	$order_product_details = $statement->execute();
	$statement->closeCursor();
        
        return $order_product_details;
    }
}
