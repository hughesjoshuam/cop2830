<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * April 22, 2017
 * 
 * Final Project: Categories.php - Categories Model
 */


/**
 * PK categoryID INT(11)
 * categoryName VARCHAR(255)
 */
class Categories {
    public function Select($categoryID = null, $orderBy = 'categoryID'){
        global $db;
        $query = 'SELECT * FROM categories WHERE 1 = 1 ';
        if($categoryID != null){ $query = $query . 'AND categoryID = :categoryID '; }        
        $query = $query . 'ORDER BY :orderBy';
        
        $statement = $db->prepare($query);
        if($categoryID != null){ $statement->bindValue(":categoryID", $categoryID); }
        $statement->bindValue(":orderBy", $orderBy);
	$statement->execute();
	$categories = $statement->fetchAll();
	$statement->closeCursor();
	
        return $categories;
    }
    
    public function Insert($categoryName){
        global $db;
	$query = 'INSERT INTO categories (categoryName) '
                . 'VALUES (:categoryName)';
	
	$statement = $db->prepare($query);
        $statement->bindValue(":categoryName", $categoryName);
	$categories = $statement->execute();
	$statement->closeCursor();
        
        return $categories;
    }
    
    public function Update($categoryID, $categoryName){
        global $db;
        $query = 'UPDATE categories '
                . 'SET categoryName = :categoryName '
                . 'WHERE categoryID = :categoryID ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":categoryID", $categoryID);
        $statement->bindValue(":categoryName", $categoryName);
	$categories = $statement->execute();
	$statement->closeCursor();
        
        return $categories;
    }
    
    public function Delete($categoryID){
        global $db;
        $query = 'DELETE FROM categories WHERE categoryID = :categoryID ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":categoryID", $categoryID);
	$categories = $statement->execute();
	$statement->closeCursor();
        
        return $categories;
    }
}
