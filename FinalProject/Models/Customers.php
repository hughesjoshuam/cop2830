<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: Customers - Customers Model
 *
 */

/**
 * PK customerID INT(11)
 * customerName VARCAHR(45)
 * customerStreetAddress VARCHAR(255)
 * customerCity VARCHAR(45)
 * customerState VARCHAR(2)
 * customerPostalCode VARCHAR(4)
 */
class Customers {
    
    public function Select($customerID = null, $orderBy = 'categoryID'){
        global $db;
        $query = 'SELECT * FROM customers WHERE 1 = 1 ';        
        if($customerID != null){ $query = $query . 'AND customerID = :customerID '; }
        $query = $query . 'ORDER BY :orderBy';
        
        $statement = $db->prepare($query);
        if($customerID != null){ $statement->bindValue(":customerID", $customerID); }
        $statement->bindValue(":orderBy", $orderBy);
	$statement->execute();
	$customers = $statement->fetchAll();
	$statement->closeCursor();
        
	return $customers;
    }
    
    public function Insert($customerName, $customerStreetAddress, $customerCity, $customerState, $customerPostalCode){
        global $db;
	$query = 'INSERT INTO customers (customerName, customerStreetAddress, customerCity, customerState, customerPostalCode) '
                . 'VALUES (:customerName, :customerStreetAddress, :customerCity, :customerState, :customerPostalCode) ';
	
	$statement = $db->prepare($query);
        $statement->bindValue(":customerName", $customerName);
        $statement->bindValue(":customerStreetAddress", $customerStreetAddress);
        $statement->bindValue(":customerCity", $customerCity);
        $statement->bindValue(":customerState", $customerState);
        $statement->bindValue(":customerPostalCode", $customerPostalCode);
	$customers = $statement->execute();
	$statement->closeCursor();
        
        return $customers;
    }
    
    public function Update($customerID, $customerName, $customerStreetAddress, $customerCity, $customerState, $customerPostalCode){
        global $db;
        $query = 'UPDATE customers '
                . 'SET customerName = :customerName, customerStreetAddress = :customerStreetAddress, customerCity = :customerCity, '
                . 'customerState = :customerState, customerPostalCode = :customerPostalCode '
                . 'WHERE customerID = :customerID ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":customerID", $customerID);
        $statement->bindValue(":customerName", $customerName);
        $statement->bindValue(":customerStreetAddress", $customerStreetAddress);
        $statement->bindValue(":customerCity", $customerCity);
        $statement->bindValue(":customerState", $customerState);
        $statement->bindValue(":customerPostalCode", $customerPostalCode);
	$statement->execute();
	$statement->closeCursor();
        
        return $statement;
    }
    
    public function Delete($customerID){
        global $db;
        $query = 'DELETE FROM customers WHERE customerID = :customerID ';
        
        $statement = $db->prepare($query);
        $statement->bindValue(":customerID", $customerID);
	$statement->execute();
	$statement->closeCursor();
        
        return $statement;
    }
}
