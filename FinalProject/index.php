<?php
/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * April 20, 2017
 * Final Project: Index.php - Application Initialization
*/

// Load Application Start Configurations
foreach (glob("App_Start/*.php") as $filename)
{
    file_exists($filename) ? require_once $filename : $filename . ' : file not found.';
}

// Chose to establish session here even though authentication is not required
Session::Initialize();
AppConfig::Initialize();

// Load Models
foreach (glob("Models/*.php") as $filename)
{
    file_exists($filename) ? require_once $filename : $filename . ' : file not found.';
}

// Load Controllers
foreach (glob("Controllers/*.php") as $filename)
{
    file_exists($filename) ? require_once $filename : $filename . ' : file not found.';
}

// Load RouteMap based upon $_SERVER['REQUEST_URI']
$RouteMap = RouteControl();
$controller = $RouteMap['controller'];
$view = $RouteMap['view'];
$id = $RouteMap['id'];

// Instantiate Controller and Model per RouteMap
if(! class_exists($controller)){ echo $controller . ' Not Loaded.'; exit; }
$model = $controller::$view($id);

// Load View
$view = '/Views/' . $controller . '/' . $view . '.php';
require_once('/views/Shared/_MasterLayout.php'); // Include Master Layout File

/* Enable the lines below for debugging information
 * Note: Debug Code may result in unexpected behaviour in Views.
var_dump(get_defined_vars());
phpinfo();
*/

?>

    