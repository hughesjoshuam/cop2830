<?php

/* 
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * April 20, 2017
 * 
 * Final Project: _Header.php - Partial View for Header.
 */

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $GLOBALS['APPLICATION_TITLE']; ?></title>
        
        <link rel="stylesheet" href="/Content/main.css" type="text/css" />
        
    </head>
    <body>
        <div id="header">
            <h1><?php echo $GLOBALS['APPLICATION_TITLE']; ?></h1>
            <h2><?php echo $_SESSION['Page Title']; ?></h2>
        </div>