<?php
/* 
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * April 20, 2017
 * 
 * Final Project: _MasterLayout.php - Master application layout
 */

// Include the Header partial view
require_once('_Header.php');

// Include the Navigation partial view
require_once('_Navigation.php');

/* 
 * Add Code here for injecting page content from View 
 * Develop function for calling the master layout page? 
 */
?>

<div id="main">
    <div id="section">
        <?php
        // Include the main view
        include($view);
        ?>
    </div>
</div>

<?php
// Include the Footer partial view
require_once('_Footer.php');
?>
