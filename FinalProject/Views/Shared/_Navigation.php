<?php

/* 
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * April 20, 2017
 * 
 * Final Project: _Navigation.php - Partial View for Navigation.
 */

?>

<!-- Aside -->
<div id="aside">
    <!-- Navigation -->
    <div id="nav">
        <ul>
            <li><a href="/Home" class="level1">Product List</a></li>
            <!-- These items will be restricted to logged on Admin users in the future -->
            <li><a href="/ProductManager">Product Manager</a></li>
            <li><a href="/CategoryManager">Category Manager</a></li>
            <li><a href="/CustomerManager">Customer Manager</a></li>
            <li><a href="/OrderManager">Order Manager</a></li>
            <li><a href="/Home/Repair/">Instrument Repair</a></li>
            <li><a href="/Home/About/">About Us</a></li>
        </ul>
        <hr />
    </div>
    <!-- Product List Sub Menu-->
    <div id="nav">
        <ul>
        <?php 
        if ($controller == "Home")
        {
        ?>
            <li><a href="/Home" class="level1">All Categories</a></li>
        <?php 
            $Categories = new Categories();
            $categories = $Categories->Select();
            foreach($categories as $category)
            {
        ?>
            <li><a href="/Home/Index/<?php echo $category['categoryID']; ?>" class="level1"><?php echo $category['categoryName']; ?></a></li>
        <?php
            }
        }
        else if ($controller == "ProductManager")
        {
        ?>
            <!-- Product Manager Sub Menu -->
            <li><a href="/ProductManager/AddProduct/">Add Product</a></li>
            <hr />
            <li><a href="/ProductManager" class="level1">All Categories</a></li>
        <?php
            $Categories = new Categories();
            $categories = $Categories->Select();
            foreach($categories as $category)
            {
        ?>
            <li><a href="/ProductManager/Index/<?php echo $category['categoryID']; ?>" class="level1"><?php echo $category['categoryName']; ?></a></li>
        <?php
            }
        }
        else if ($controller == "CustomerManager")
        {
        ?>
            <!-- Customer Manager Sub Menu -->
            <li><a href="/CustomerManager/AddCustomer/">Add Customer</a></li>
        <?php        
        }
        else if ($controller == "OrderManager")
        {
        ?>
        <!-- Order Manager Sub Menu -->
        <li><a href="/OrderManager/AddOrder/">Add Order</a></li>
        <?php        
        }
        else if ($controller == "CategoryManager")
        {
        ?>
        <!-- Category Manager Sub Menu -->
        <li><a href="/CategoryManager/AddCategory/">Add Category</a></li>
        <?php
        }
        ?>
        </ul>
    </div>
</div>