<?php

/* 
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * April 20, 2017
 * 
 * Final Project: _Footer.php - Partial View for footer.    
 */

?>
        </div>
        <div id="footer">
            <address>
                <p>
                    &copy;<?php echo $GLOBALS['APPLICATION_TITLE']; ?><br />
                    <br />
                    Joshua M. Hughes <br />
                    COP2830: INTERNET PROGRAMMING HTML II 101 <br />
                    Professor Barrell <br />
                </p>
            </address>
        </div>
    </body>
</html>

