<?php

/* 
 * Joshua M. Hughes
 * COP28/30
 * Professor Barrell
 * April 20, 2017
 * 
 * Final Project: Index.php - Home => Index View - Main Page
 * 
 */

?>

<div>
    <h3>
        Welcome to the Guitar Store!
    </h3>
    <h4>
        Select a product from our product list to view its details. 
    </h4>
</div>
    <table>
        <tr>
            <th>Category</th>
            <th>Name</th>
            <th>Price</th>
            <th></th>
        </tr>
            <?php
            if($model != null){
                foreach ($model as $modelItem)
                {
                ?>
                    <tr>
                        <td><?php echo $modelItem['categoryName']; ?></td>
                        <td><?php echo $modelItem['productName']; ?></td>
                        <td><?php echo $modelItem['listPrice']; ?></td>
                        <td>
                            <a class = "tableAction" href="/Home/ProductDetails/<?php echo $modelItem['productID']; ?>">Details</a>
                        </td>
                    </tr>
                <?php
                }  
            }
            ?> 
    </table>
</div>
