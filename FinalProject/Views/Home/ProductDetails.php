<?php

/* 
 * Joshua M. Hughes
 * COP28/30
 * Professor Barrell
 * April 20, 2017
 * 
 * Final Project: Index.php - Home => ProductDetails View - Main Page
 * 
 */

?>
<?php
foreach ($model as $modelItem)
{
?>
<div>
    <img class="productImage" src="/Images/<?php echo $modelItem['productCode']; ?>.jpg" alt="<?php echo $modelItem['productCode']; ?>" />
</div>
<div>
    <dl class="dl-horizontal">
        <dt>
            Category Name:
        </dt>
        <dd>
            <?php echo $modelItem['categoryName']; ?>
        </dd>
        <dt>
            Product Code:
        </dt>
        <dd>
            <?php echo $modelItem['productCode']; ?>
        </dd>
        <dt>
            Product Name:
        </dt>
        <dd>
            <?php echo $modelItem['productName']; ?>
        </dd>
        <dt>
            List Price:
        </dt>
        <dd>
            <?php echo $modelItem['listPrice']; ?>
        </dd>
    </dl>
</div>
<?php   
}
?>
<p>
    <a class = "tableAction" href="/Home">Back to List</a>
</p>