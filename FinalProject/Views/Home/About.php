<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 26, 2017
 *
 * FinalProject: About - About Us View
 *
 */

?>

<div>
    <p>
    The Guitar Shop offers a wide variety of instruments. We specialize in guitars 
    and guitar repair. We also offer pianos, violins, and other string instruments. 
    Our instrument repair shop has knowledgeable staff, and is equipped to repair most
    string instruments. 
    </p>
</div>