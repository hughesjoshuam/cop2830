<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 26, 2017
 *
 * FinalProject: Repair - Instrument Repair View
 *
 */

?>

<div>
    <p>
        Our instrument repair shop is open Monday through Friday from 7 AM - 6 PM. 
        We accept walk-ins and appointments for instrument repair. Most repairs 
        will be completed next business day depending on season. Same day emergency
        repair services are available for an additional fee. All repairs are warantied
        for six months from the date the repair is completed.
        
        To schedule an appointment with our repair shop you can contact us at: 867-5309.
    </p>
    <br />
    <img src="/Images/repair.jpg" alt="Repair Shop Image" />
</div>