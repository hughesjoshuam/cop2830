<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: AddCategory - Add Category View
 *
 */

?>

<form name="AddCategory" method="post">
    <div class="form-horizontal">
        <div class="form-group">
            Category Name
            <div class="col-md-10">
                <input class = "form-control" type="text" name="categoryName" id="categoryName">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Create" class="btn btn-default" />
            </div>
        </div>
    </div>
</form>
<div>
    <br />
    <a href="\CategoryManager\Index">Back to List</a>
</div>
