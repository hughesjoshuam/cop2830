<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: DeleteCategory - Delete Category View
 *
 */

?>
<div>
    <h3>Are you sure you want to delete this?</h3>
    <form name="DeleteCategory" method="post">
    <?php 
    foreach ($model as $modelItem)
    {
        ?>
        <div>
            <dl class="dl-horizontal">
                Category Name: <?php echo $modelItem['categoryName']; ?>
                <input type="hidden" name="confirm" id="confirm" value="true" />
            </dl>
            <div class="form-actions no-color">
                <input type="submit" value="Delete" class="btn btn-default" /> |
                <a href="\CategoryManager\Index">Back to List</a>
            </div>
        </div>
        <?php
    }
    ?>
    </form>
</div>
