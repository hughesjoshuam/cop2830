<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: Index - Category Manager Main View
 *
 */

?>
<div>
    <table class="table">
    <tr>
        <th>
            Category Name
        </th>
        <th>Action</th>
    </tr>
<?php if ($model != null)
{
    foreach ($model as $item)
    {
    ?>
    <tr>
        <td>
            <?php echo $item['categoryName']; ?>
        </td>
        <td>
            <a class="tableAction" href="\CategoryManager\EditCategory\<?php echo $item['categoryID']; ?>">Edit</a> |
            <a class="tableAction" href="\CategoryManager\DeleteCategory\<?php echo $item['categoryID']; ?>">Delete</a>
        </td>
    </tr>
    <?php
    }
}
?>
</table>