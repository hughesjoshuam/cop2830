<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: EditCategory - Edit Category View
 *
 */

?>
<div>
    <form name="EditCategory" method="post">
        <?php 
        foreach ($model as $modelItem){
        ?>
        <div class="form-horizontal">
        <input type="hidden" name="categoryID" id="categoryID" value="<?php echo $modelItem['categoryID']; ?>" />

        <div class="form-group">
            Category Name
            <div class="col-md-10">
                <input class = "form-control" type="text" name="categoryName" id="categoryName  " value="<?php echo $modelItem['categoryName']; ?>" />
            </div>
        </div>
        <?php
        }
        ?>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Save" class="btn btn-default" />
            </div>
        </div>
        
    </form>
</div>
<div>
    <br />
    <a href="\CategoryManager\Index">Back to List</a>
</div>
