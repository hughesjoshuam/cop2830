<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: Index - Customer Manager Main View
 *
 */

?>

<table class="table">
    <tr>
        <th>Customer Name</th>
        <th>Street Address</th>
        <th>City</th>
        <th>State</th>
        <th>Zip Code</th>
        <th>Action</th>
    </tr>
<?php
if ($model != null)
{
    foreach ($model as $modelItem)
    {
        ?>
        <tr>
            <td>
                <?php echo $modelItem['customerName']; ?>
            </td>
            <td>
                <?php echo $modelItem['customerStreetAddress']; ?>
            </td>
            <td>
                <?php echo $modelItem['customerCity']; ?>
            </td>
            <td>
                <?php echo $modelItem['customerState']; ?>
            </td>
            <td>
                <?php echo $modelItem['customerPostalCode']; ?>
            </td>
            <td>
                <a class="tableAction" href="\CustomerManager\EditCustomer\<?php echo $modelItem['customerID']; ?>">Edit</a> |
                <a class="tableAction" href="\CustomerManager\DeleteCustomer\<?php echo $modelItem['customerID']; ?>">Delete</a>
            </td>
        </tr>
        <?php
    }
}
?>
</table>