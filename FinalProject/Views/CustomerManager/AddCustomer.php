<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: AddCustomer - Add Customer View
 *
 */

?>
<form name="AddCustomer" method="post">
    <div class="form-horizontal">
        <div class="form-group">
            Customer Name
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerName" id="customerName" />
            </div>
        </div>

        <div class="form-group">
            Street Address
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerStreetAddress" id="customerStreetAddress" />
            </div>
        </div>

        <div class="form-group">
            City
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerCity" id="customerCity" />
            </div>
        </div>

        <div class="form-group">
            State
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerState" id="customerState" />
            </div>
        </div>

        <div class="form-group">
            Zip Code
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerPostalCode" id="customerPostalCode" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Create" class="btn btn-default" />
            </div>
        </div>
    </div>
</form>
<div>
    <br />
    <a href="\CustomerManager\Index">Back to List</a>
</div>

