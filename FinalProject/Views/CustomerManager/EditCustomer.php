<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: EditCustomer - Edit Customer View
 *
 */

?>
<form name="EditCustomer" method="post">
    <?php
    foreach ($model as $modelItem)
    {
    ?>    
    <input class = "form-control" type="hidden" name="customerID" id="customerID" value="<?php echo $modelItem['customerID']; ?>" />
    <div class="form-horizontal">
        <div class="form-group">
            Customer Name
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerName" id="customerName" value="<?php echo $modelItem['customerName']; ?>" />
            </div>
        </div>

        <div class="form-group">
            Street Address
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerStreetAddress" id="customerStreetAddress" value="<?php echo $modelItem['customerStreetAddress']; ?>" />
            </div>
        </div>

        <div class="form-group">
            City
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerCity" id="customerCity" value="<?php echo $modelItem['customerCity']; ?>" />
            </div>
        </div>

        <div class="form-group">
            State
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerState" id="customerState" value="<?php echo $modelItem['customerState']; ?>" />
            </div>
        </div>

        <div class="form-group">
            Zip Code
            <div class="col-md-10">
                <input class = "form-control" type="text" name="customerPostalCode" id="customerPostalCode" value="<?php echo $modelItem['customerPostalCode']; ?>" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Save" class="btn btn-default" />
            </div>
        </div>
    </div>
    <?php
    }
?>
</form>
<div>
    <br />
    <a href="\CustomerManager\Index">Back to List</a>
</div>