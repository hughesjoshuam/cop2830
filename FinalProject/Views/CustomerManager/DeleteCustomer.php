<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: DeleteCustomer - Delete Customer View
 *
 */

?>
<h3>Are you sure you want to delete this?</h3>
<?php 
    foreach ($model as $modelItem)
    {
        ?>
<div>
    <dl class="dl-horizontal">
        <dt>
            Customer Name:
        </dt>
        <dd>
            <?php echo $modelItem['customerName']; ?>
        </dd>

        <dt>
            Street Address:
        </dt>

        <dd>
            <?php echo $modelItem['customerStreetAddress']; ?>
        </dd>

        <dt>
            City: 
        </dt>

        <dd>
            <?php echo $modelItem['customerCity']; ?>
        </dd>

        <dt>
            State: 
        </dt>

        <dd>
            <?php echo $modelItem['customerState']; ?>
        </dd>

        <dt>
            <?php echo $modelItem['customerPostalCode']; ?>
        </dt>

        <dd>
            <?php echo $modelItem['customerPostalCode']; ?>
        </dd>
    </dl>
    <form name="DeleteCustomer" method="post">
        <div class="form-actions no-color">
            <input type="hidden" name="customerID" id="customerID" value<?php echo $modelItem['customerID']; ?> />
            <input type="submit" value="Delete" class="btn btn-default" /> |
            <a href="\CustomerManager\Index">Back to List</a>
        </div>
    </form>
    <?php
    }
    ?>
</div>
