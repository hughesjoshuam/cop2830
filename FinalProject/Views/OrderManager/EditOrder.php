<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: EditOrder - Edit Order View
 *
 */

?>
<form name="EditOrder" method="post">
    <?php
    foreach($model as $modelItem){
    ?>
    <div class="form-horizontal">
        <div class="form-group">
            Customer:
            <div class="col-md-10">
                <select class="form-control" name="customerID" id="customerID">
                    <?php
                    $Customers = new Customers();
                    $customers = $Customers->Select();
                    foreach($customers as $customer){
                    ?>
                    <option value="<?php echo $customer['customerID']; ?>" <?php $customer['customerID'] == $modelItem['customerID'] ? print 'selected = "selected"' : null; ?>>
                            <?php echo $customer['customerName']; ?>
                        </option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            Order Date:
            <div class="col-md-10">
                <input class="form-control" type="text" name="orderDate" id="orderDate" value="<?php echo date("m/d/Y h:i A", strtotime($modelItem['orderDate'])); ?>" />
            </div>
        </div>

        <div class="form-group">
            Description:
            <div class="col-md-10">
                <input class="form-control" type="text" name="orderDescription" id="orderDescription" value="<?php echo $modelItem['orderDescription']; ?>" />
            </div>
        </div>
        <div class="form-group">
            Products Ordered:
            <div class="col-md-10">
                <div class="col-md-10">
                    <table>
                        <tr>
                            <th>Qty</th>
                            <th>Category</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Price</th>
                        </tr>
                        <?php
                        $Order_Product_Details = new Order_Product_Details();
                        $order_product_details = $Order_Product_Details->Select($modelItem['orderID']);
                        foreach ($order_product_details as $order_product_detail)
                        {
                        ?>
                        <tr>
                            <td>
                                <input class="form-control" type="hidden" name="productID[]" id="productID_<?php echo $order_product_detail['productID']; ?>" value="<?php echo $order_product_detail['productID']; ?>" />
                                <input class="quantity-form-control" type="text" name="productQty[]" id="productQty<?php echo $order_product_detail['productID']; ?>" value="<?php echo $order_product_detail['quantity']; ?>" />
                            </td>
                            <td><?php echo $order_product_detail['categoryName']; ?></td>
                            <td><?php echo $order_product_detail['productCode']; ?></td>
                            <td><?php echo $order_product_detail['productName']; ?></td>
                            <td><?php echo $order_product_detail['listPrice']; ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
          </div>
    </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Update" class="btn btn-default" />
            </div>
        </div>
    </div>
    <?php
    }
    ?>
</form>
<p>
           <a href="/OrderManager/Index">Back to List</a>
</p>