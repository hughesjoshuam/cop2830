<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: DeleteOrder - Delete Order View
 *
 */

?>
<?php
foreach($model as $modelItem){
?>
<h3>Are you sure you want to delete this?</h3>
<div>
    <dl class="dl-horizontal">
        <dt>
            Customer Name:
        </dt>

        <dd>
            <?php echo $modelItem['customerName']; ?>
        </dd>

        <dt>
            Order Date:
        </dt>

        <dd>
            <?php echo $modelItem['orderDate']; ?>
        </dd>

        <dt>
            Order Description:
        </dt>

        <dd>
            <?php echo $modelItem['orderDescription']; ?>
        </dd>

    </dl>
    <form name="DeleteOrder" method="post">
        <div class="form-actions no-color">
            <input type="hidden" name="orderID" id="orderID" value="<?php echo $modelItem['orderID']; ?>" />
            <input type="submit" value="Delete" class="btn btn-default" /> |
            <a href="/OrderManager/Index">Back to List</a>
        </div>
    </form>
<?php
}
?>
</div>