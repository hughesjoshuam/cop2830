<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: OrderDetails - Order Details View
 *
 */

?>
<?php
foreach($model as $modelItem){
?>
<div>
    <dl class="dl-horizontal">
        <dt>
            Customer Name:
        </dt>

        <dd>
            <?php echo $modelItem['customerName']; ?>
        </dd>

        <dt>
            Order Date:
        </dt>

        <dd>
            <?php echo date("m/d/Y h:i A", strtotime($modelItem['orderDate'])); ?>
        </dd>

        <dt>
            Order Description:
        </dt>

        <dd>
            <?php echo $modelItem['orderDescription']; ?>
        </dd>

        <dt>
            Products Ordered:
        </dt>
    </dl>
</div>
<div class="col-md-10">
    <table>
        <tr>
            <th>Qty</th>
            <th>Category</th>
            <th>Code</th>
            <th>Name</th>
            <th>Price</th>
        </tr>
        <?php
        $Order_Product_Details = new Order_Product_Details();
        $order_product_details = $Order_Product_Details->Select($modelItem['orderID']);
        foreach ($order_product_details as $order_product_detail)
        {
        ?>
        <tr>
            <td><?php echo $order_product_detail['quantity']; ?></td>
            <td><?php echo $order_product_detail['categoryName']; ?></td>
            <td><?php echo $order_product_detail['productCode']; ?></td>
            <td><?php echo $order_product_detail['productName']; ?></td>
            <td><?php echo $order_product_detail['listPrice']; ?></td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>
<p>
    <a href="/OrderManager/EditOrder/<?php echo $modelItem['orderID']; ?>">Edit</a> |
    <a href="/OrderManager/Index">Back to List</a>
</p>
<?php
}
?>