<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: Index - Order Manager Main View
 *
 */

?>
<table class="table">
    <tr>
        <th>
            Customer
        </th>
        <th>
            Order Date
        </th>
        <th>
            Order Description
        </th>
        <th></th>
    </tr>
<?php
if($model != null) {
    foreach ($model as $modelItem)
    {
    ?>
    <tr>
        <td>
            <?php echo $modelItem['customerName']; ?>
        </td>
        <td>
            <?php echo date("m/d/Y h:i A", strtotime($modelItem['orderDate'])); ?>
        </td>
        <td>
            <?php echo $modelItem['orderDescription']; ?>
        </td>
        <td>
            <a class="tableAction" href="/OrderManager/EditOrder/<?php echo $modelItem['orderID']; ?>">Edit</a> |
            <a class="tableAction" href="/OrderManager/OrderDetails/<?php echo $modelItem['orderID']; ?>">Details</a> |
            <a class="tableAction" href="/OrderManager/DeleteOrder/<?php echo $modelItem['orderID']; ?>">Delete</a> 
        </td>
    </tr>
    <?php
    }
}
?>
</table>