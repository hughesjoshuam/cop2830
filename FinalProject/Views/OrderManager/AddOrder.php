<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: AddOrder - Add Order View
 *
 */

?>
<form name="AddOrder" method="Post">
    <div class="form-horizontal">
        <div class="form-group">
           Customer:
            <div class="col-md-10">
                <select class="form-control" name="customerID" id="customerID">
                    <option></option>
                    <?php
                    $Customers = new Customers();
                    $customers = $Customers->Select();
                    foreach($customers as $customer){
                    ?>
                    <option value="<?php echo $customer['customerID']; ?>"><?php echo $customer['customerName']; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            Order Date:
            <div class="col-md-10">
                <input class = "form-control" type="text" name="orderDate" id="orderDate" value="<?php echo date("m/d/Y h:i A"); ?>"/>
            </div>
        </div>

        <div class="form-group">
            Description:
            <div class="col-md-10">
                <input class="form-control" type="text" name="orderDescription" id="orderDescription" />
            </div>
        </div>
        <div class="form-group">
            Select Products:
            <div class="col-md-10">
                <table>
                    <tr>
                        <th>Qty</th>
                        <th>Category</th>   
                        <th>Code</th>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                    <?php
                    $Products = new Products();
                    $products = $Products->Select();
                    foreach ($products as $product)
                    {
                    ?>
                        <tr>
                            <td>
                                <input type="hidden" name="productID[]" id="productID_<?php echo $product['productID']; ?>" value="<?php echo $product['productID']; ?>" />
                                <input class="quantity-form-control" type="text" name="productQty[]" id="productQty_<?php echo $product['productID']; ?>" />
                            </td>
                            <td><?php echo $product['categoryName']; ?></td>
                            <td><?php echo $product['productCode']; ?></td>
                            <td><?php echo $product['productName']; ?></td>
                            <td><?php echo $product['listPrice']; ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Create" class="btn btn-default" />
            </div>
        </div>
    </div>
</form>
<div>
    <a href="/OrderManager/Index">Back to List</a>
</div>