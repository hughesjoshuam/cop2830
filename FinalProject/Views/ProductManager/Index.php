<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: Index - Product Manager Main View
 *
 */
 ?>
<table class="table">
    <tr>
        <th>
            Category Name
        </th>
        <th>
            Product Code
        </th>
        <th>
            Product Name
        </th>
        <th>
            List Price
        </th>
        <th>Action</th>
    </tr>

<?php 
if ($model != null)
{
    foreach ($model as $modelItem)
    {
        ?>
        <tr>
            <td>
                <?php echo $modelItem['categoryName']; ?>
            </td>
            <td>
                <?php echo $modelItem['productCode']; ?>
            </td>
            <td>
                <?php echo $modelItem['productName']; ?>
            </td>
            <td>
                <?php echo $modelItem['listPrice']; ?>
            </td>
            <td>
                <a class="tableAction" href="\ProductManager\EditProduct\<?php echo $modelItem['productID']; ?>">Edit</a> |
                <a class="tableAction" href="\ProductManager\DeleteProduct\<?php echo $modelItem['productID']; ?>">Delete</a>
            </td>
        </tr>
        <?php
    }
}
?>
</table>
