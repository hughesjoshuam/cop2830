<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: DeleteProduct - Delete Product View
 *
 */
?>
<?php
foreach($model as $modelItem)
{
?>
<h3>Are you sure you want to delete this?</h3>
<div>
    <dl class="dl-horizontal">
        <dt>
            Category Name: 
        </dt>

        <dd>
            <?php echo $modelItem['categoryName']; ?>
        </dd>

        <dt>
            Product Code: 
        </dt>

        <dd>
            <?php echo $modelItem['productCode']; ?>
        </dd>

        <dt>
            Product Name: 
        </dt>

        <dd>
            <?php echo $modelItem['productName']; ?>
        </dd>

        <dt>
            List Price: 
        </dt>

        <dd>
            <?php echo $modelItem['listPrice']; ?>
        </dd>

    </dl>

    <form name="DeleteProduct" method="post">
        <div class="form-actions no-color">
            <input type="hidden" name="productID" id="productID" value="<?php echo $modelItem['productID']; ?>" />
            <input type="submit" value="Delete" class="btn btn-default" /> |
            <a href="\ProductManager\Index">Back to List</a>
        </div>
    </form>
<?php
}
?>
</div>