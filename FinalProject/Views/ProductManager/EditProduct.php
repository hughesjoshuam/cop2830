<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: EditProduct - Edit Product View
 *
 */
?>
<form name="EditProduct" method="post">
<?php
foreach ($model as $modelItem)
    {
    ?>    
    <div class="form-horizontal">
        <input type="hidden" name="productID" id="productID" value="<?php echo $modelItem['productID']; ?>" />

        <div class="form-group">
            Category Name:
            <div class="col-md-10">
                <select class="form-control" name="categoryID">
                <?php
                    $Categories = new Categories();
                    $categories = $Categories->Select();
                    foreach($categories as $category){
                        ?>
                    <option value="<?php echo $category['categoryID']; ?>" <?php $category['categoryID'] == $modelItem['categoryID'] ? print 'selected = "selected"' : null; ?>><?php echo $category['categoryName']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            Product Code: 
            <div class="col-md-10">
                <input class = "form-control" type="text" name="productCode" id="productCode" value="<?php echo $modelItem['productCode']; ?>" />
            </div>
        </div>
        <div class="form-group">
            Product Name: 
            <div class="col-md-10">
                <input class = "form-control" type="text" name="productName" id="productName" value="<?php echo $modelItem['productName']; ?>" />
            </div>
        </div>

        <div class="form-group">
            List Price: 
            <div class="col-md-10">
                <input class = "form-control" type="text" name="listPrice" id="listPrice" value="<?php echo $modelItem['listPrice']; ?>" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Save" class="btn btn-default" />
            </div>
        </div>
    </div>
    <?php
    }
?>
</form>
<div>
    <br />
    <a href="\ProductManager\Index">Back to List</a>
</div>
