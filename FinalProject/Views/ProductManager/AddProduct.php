<?php

/*
 * Joshua M. Hughes
 * COP2830
 * Professor Barrell
 * Apr 23, 2017
 *
 * FinalProject: AddProduct - Add Product View
 *
 */
?>
<form name="AddProduct" method="post">
    <div class="form-horizontal">
        <div class="form-group">
            Category Name:
            <div class="col-md-10">
                <select class = "form-control" name="categoryID">
                    <?php
                        $Categories = new Categories();
                        $categories = $Categories->Select();
                        foreach ($categories as $category){
                            ?>
                    <option value="<?php echo $category['categoryID']; ?>"><?php echo $category['categoryName']; ?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            Product Code: 
            <div class="col-md-10">
                <input class = "form-control" type="text" name="productCode" id="productCode" />
            </div>
        </div>

        <div class="form-group">
            Product Name: 
            <div class="col-md-10">
                <input class = "form-control" type="text" name="productName" id="productName" />
            </div>
        </div>

        <div class="form-group">
            List Price: 
            <div class="col-md-10">
                <input class = "form-control" type="text" name="listPrice" id="listPrice" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Create" class="btn btn-default" />
            </div>
        </div>
        * Image publishing not available at this time. Images must be uploaded via FTP.
    </div>
</form>
<div>
    <br />
    <a href="\ProductManager\Index">Back to List</a>
</div>

