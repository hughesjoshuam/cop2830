<?php 
/* Joshua M. Hughes
 * COP2830
 * 3/30/2017
 * Professor Barrell
 * 
 * index.php file
 * Chapter 7 Murach PHP & MySQl
 */
include 'header.php'; 

$intCountDown = filter_input(INPUT_GET, 'intCountDown');
?>

<main>      
    <h1>Chapter 8 - In Class Walk Through</h1>
    <h2>Switch Statements</h2>
    <section>
        <div>
            <p>
            <div id="countDownClock"></div>
                <script>
                <?php
                switch($intCountDown){
                    case 10:
                        for($i = $intCountDown; $i <= 10; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    case 9:
                        for($i = $intCountDown; $i <= 9; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    case 8:
                        for($i = $intCountDown; $i <= 8; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    case 7:
                        for($i = $intCountDown; $i <= 7; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    case 6:
                        for($i = $intCountDown; $i <= 6; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    case 5:
                        for($i = $intCountDown; $i <= 5; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    case 4:
                        for($i = $intCountDown; $i <= 4; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    case 3:
                        for($i = $intCountDown; $i <= 3; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    case 2:
                        for($i = $intCountDown; $i <= 2; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    case 1:
                        for($i = $intCountDown; $i <= 1; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                    default:
                        for($i = $intCountDown; $i <= 10; $i--){
                            ?>document.getElementById('countDownClock').innerHTML=<?php echo $i; ?>;<?php
                        }
                        break;
                }
                ?>
                </script>
            </p>
        </div>
    </section>
</main>
<?php include 'footer.php'; ?>