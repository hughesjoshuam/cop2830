<?php 
/* Joshua M. Hughes
 * COP2830
 * 4/6/2017
 * Professor Barrell
 * 
 * index.php file
 * Chapter 78Murach PHP & MySQl
 */
include 'header.php'; 

?>

<main>      
    <h1>Chapter 8 - In Class Walk Through</h1>
    <!-- Display a list of forms -->
    <h2>Control Statements</h2>
    <section>
        
        <!---Equality Operators--->
        <article>
            <h3>Equality Operators</h3>
            <table>
                <tr>
                    <th>Operator</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>==</td>
                    <td>Equal</td>
                    <td>$lastname == 'Harris'</td>
                    <td><?php $lastname = 'Harris'; ?>$lastname == 'Harris': is <?php echo ($lastname == 'Harris') ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>!=</td>
                    <td>Not Equal</td>
                    <td>$months != 0</td>
                    <td><?php $months =4; ?>$months != 0: is <?php echo ($months != 0) ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td><></td>
                    <td>Not Equal</td>
                    <td>$months <> 0</td>
                    <td><?php $months =4; ?>$months <> 0: is <?php echo ($months <> 0) ? 'true' : 'false'; ?></td>
                </tr>
            </table>
        </article>
        <article>
            <h3>Unusual Results with the Equality Operators</h3>
            <table>
                <tr>
                    <th>Expression</th>
                    <th>Result</th>
                    <th>Description</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>NULL == ''</td>
                    <td>TRUE</td>
                    <td>NULL is converted to the empty string</td>
                    <td>
                        NULL == '': is <?php echo (NULL == '') ? 'true' : 'false'; ?><br />
                        NULL === '': is <?php echo (NULL === false) ? 'true' : 'false'; ?><br />
                    </td>
                </tr>
                <tr>
                    <td>NULL == false</td>
                    <td>TRUE</td>
                    <td>NULL is converted to FALSE</td>
                    <td>
                        NULL == false: is <?php echo (NULL == false) ? 'true' : 'false'; ?><br />
                        NULL === false: is <?php echo (NULL === false) ? 'true' : 'false'; ?><br />
                    </td>
                </tr>
                <tr>
                    <td>NULL == 0</td>
                    <td>TRUE</td>
                    <td>NULL is equal to any value that evaluates to FALSE</td>
                    <td>
                        NULL == 0: is <?php echo (NULL == 0) ? 'true' : 'false'; ?><br />
                        NULL === 0: is <?php echo (NULL == 0) ? 'true' : 'false'; ?><br />
                    </td>
                </tr>
                <tr>
                    <td>false == 0</td>
                    <td>TRUE</td>
                    <td>Empty strings and '0' are converted to FALSE</td>
                    <td>
                        false == 0: is <?php echo (false == 0) ? 'true' : 'false'; ?><br />
                        false === 0: is <?php echo (false === 0) ? 'true' : 'false'; ?><br />
                    </td>
                </tr>
                <tr>
                    <td>true == 'false'</td>
                    <td>TRUE</td>
                    <td>All other strings are converted to TRUE</td>
                    <td>
                        true == 'false': is <?php echo (true == 'false') ? 'true' : 'false'; ?><br />
                        true === 'false': is <?php echo (true === 'false') ? 'true' : 'false'; ?><br />
                    </td>
                </tr>
                <tr>
                    <td>3.5 == "\t3.5 mi"</td>
                    <td>TRUE</td>
                    <td>The string is converted to a number and then compared</td>
                    <td>
                        3.5 == "\t3.5 mi": is <?php echo (3.5 == "\t3.5 mi") ? 'true' : 'false'; ?><br />
                        3.5 === "\t3.5 mi": is <?php echo (3.5 === "\t3.5 mi") ? 'true' : 'false'; ?><br />
                    </td>
                </tr>
                <tr>
                    <td>INF == 'INF'</td>
                    <td>FALSE</td>
                    <td>The string INF is converted to 0</td>
                    <td>
                        INF == 'INF': is <?php echo (INF == 'INF') ? 'true' : 'false'; ?><br />
                        INF === 'INF': is <?php echo (INF === 'INF') ? 'true' : 'false'; ?><br />
                    </td>
                </tr>
                <tr>
                    <td>0 == ''</td>
                    <td>TRUE</td>
                    <td>The empty string is converted to 0</td>
                    <td>
                        0 == '': is <?php echo (0 == '') ? 'true' : 'false'; ?><br />
                        0 === '': is <?php echo (0 === '') ? 'true' : 'false'; ?><br />
                    </td>
                </tr>
                <tr>
                    <td>0 == 'Harris'</td>
                    <td>TRUE</td>
                    <td>Any string that is not numeric is converted to 0</td>
                    <td>
                        0 == 'Harris': is <?php echo (0 == 'Harris') ? 'true' : 'false'; ?><br />
                        0 === 'Harris': is <?php echo (0 === 'Harris') ? 'true' : 'false'; ?><br />
                    </td>
                </tr>
            </table>
        </article>
        
        <!---Identity Operators--->
        <article>
            <h3>Identity Operators</h3>
            <table>
                <tr>
                    <th>Operator</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>===</td>
                    <td>Equal</td>
                    <td>$lastname === 'Harris'</td>
                    <td><?php $lastname = 'Harris'; ?>$lastname === 'Harris': is <?php echo ($lastname === 'Harris') ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>!==</td>
                    <td>Not Equal</td>
                    <td>$months !== 0</td>
                    <td><?php $months =4; ?>$months !== 0: is <?php echo ($months !== 0) ? 'true' : 'false'; ?></td>
                </tr>
            </table>
        </article>
        
        <!---Relational Operators--->
        <article>
            <h3>Relational Operators</h3>
            <table>
                <tr>
                    <th>Operator</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td><</td>
                    <td>Less Than</td>
                    <td>$age < 18</td>
                    <td><?php $age = 18; ?>$age < 18: is <?php echo ($age < 18) ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td><=</td>
                    <td>Less Than or Equal</td>
                    <td>$investment <= 0</td>
                    <td><?php $investment =4; ?>$investment <= 0: is <?php echo ($investment <= 0) ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>></td>
                    <td>Greater Than</td>
                    <td>$test_score > 100</td>
                    <td><?php $test_score =105; ?>$test_score > 100: is <?php echo ($test_score > 100) ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>>=</td>
                    <td>Greater Than or Equal</td>
                    <td>$rate / 100 >= 0.1 <> 0</td>
                    <td><?php $rate =4; ?>$rate / 100 >= 0.1: is <?php echo ($rate / 100 >= 0.1) ? 'true' : 'false'; ?></td>
                </tr>
            </table>
        </article>
        <article>
            <h3>Comparing Strings to Numbers with Relational Operators</h3>
            <table>
                <tr>
                    <th>Expression</th>
                    <th>Result</th>
                    <th>Description</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>1 < '3'</td>
                    <td>TRUE</td>
                    <td>The string "3" is converted to the number 3</td>
                    <td>1 < '3': is <?php echo (1 < '3') ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>'10' < 3</td>
                    <td>FALSE</td>
                    <td>The string "10" is converted to the number 10</td>
                    <td>'10' < 3: is <?php echo ('10' < 3) ? 'true' : 'false'; ?></td>
                </tr>
            </table>
        </article>
        <article>
            <h3>Comparing Strings with Relational Operators</h3>
            <table>
                <tr>
                    <th>Expression</th>
                    <th>Result</th>
                    <th>Description</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>'apple' < 'orange'</td>
                    <td>TRUE</td>
                    <td>An earlier letter is less than a later letter</td>
                    <td>'apple' < 'orange': is <?php echo ('apple' < 'orange') ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>'apple' < 'appletree'</td>
                    <td>TRUE</td>
                    <td>When Characters are the same shorter strings are less than longer strings</td>
                    <td>'apple' < 'appletree': is <?php echo ('apple' < 'appletree') ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>'Orange' < 'apple'</td>
                    <td>TRUE</td>
                    <td>A capital letter is less than a lowercase letter</td>
                    <td>'Orange' < 'apple': is <?php echo ('Orange' < 'apple') ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>'@' < '$'</td>
                    <td>FALSE</td>
                    <td>Other characters are compared using their ASCII value</td>
                    <td>'@' < '$': is <?php echo ('@' < '$') ? 'true' : 'false'; ?></td>
                </tr>
            </table>
         </article>
        <article>
            <h3>Unusual Results with Relational Operators</h3>
            <table>
                <tr>
                    <th>Expression</th>
                    <th>Result</th>
                    <th>Description</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>0 <= 'test'</td>
                    <td>TRUE</td>
                    <td>The string "test" is converted to 0.</td>
                    <td>0 <= 'test': is <?php echo (0 <= 'test') ? 'true' : 'false'; ?></td>                   
                </tr>
                <tr>
                    <td>'' < 5</td>
                    <td>TRUE</td>
                    <td>The empty string is converted to 0.</td>
                    <td>'' < 5: is <?php echo (0 <= 'test') ? 'true' : 'false'; ?></td>                     
                </tr>
                <tr>
                    <td>FALSE < TRUE</td>
                    <td>TRUE</td>
                    <td>False is considered less than true</td>
                    <td>FALSE < TRUE: is <?php echo (FALSE < TRUE) ? 'true' : 'false'; ?></td>                        
                </tr>
                <tr>
                    <td>NULL < TRUE</td>
                    <td>TRUE</td>
                    <td>NULL is converted to FALSE</td>
                    <td>NULL < TRUE: is <?php echo (NULL < TRUE) ? 'true' : 'false'; ?></td>                       
                </tr>
            </table>
        </article>
        
        <!---Logical Operators--->
        <article>
            <h3>Logical Operators</h3>
            <table>
                <tr>
                    <th>Operator</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>!</td>
                    <td>NOT</td>
                    <td>!is_numeric(10)</td>
                    <td>!is_numeric(10): is <?php echo (!is_numeric(10)) ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>&&</td>
                    <td>AND</td>
                    <td>$age > 18 && $score >= 680</td>
                    <td><?php $age =19; $score=750; ?>$age > 18 && $score >= 680: is <?php echo ($age > 18 && $score >= 680) ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>||</td>
                    <td>OR</td>
                    <td>$state == 'CA' || $state == 'NC'</td>
                    <td><?php $state = 'FL'; ?>$state == 'CA' || $state == 'NC': is <?php echo ($state == 'CA' || $state == 'NC') ? 'true' : 'false'; ?></td>
                </tr>
            </table>
        </article>
        
        <!---Logical Operators : Order of Precedence--->
        <article>
            <h3>Logical Operators : Order of Precedence</h3>
            <table>
                <tr>
                    <th>Order</th>
                    <th>Operators</th>
                    <th>Description</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>!</td>
                    <td>The NOT operator</td>
                    <td>!true: is <?php echo (!true) ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td><, <=, >, >=, <></td>
                    <td>Relational Operators</td>
                    <td>!true > false: is <?php echo (!true > false) ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>==, !=, ===, !=</td>
                    <td>Equality and Identity Operators</td>
                    <td>!true > false == true: is <?php echo (!true > false == true) ? 'true' : 'false'; ?></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>&&</td>
                    <td>The AND Operator</td>
                    <td>true && true: is <?php echo (true && true) ? 'true' : 'false'; ?></td>
                </tr>                
                <tr>
                    <td>5</td>
                    <td>||</td>
                    <td>The OR Operator</td>
                    <td>true || false: is <?php echo (true || false) ? 'true' : 'false'; ?></td>
                </tr>
            </table>
        </article>
        
         <!---Logical Operators in complex condition expressions--->
        <article>
            <h3>Logical Operators</h3>
            <table>
                <tr>
                    <th>Operator</th>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>               
                <tr>
                    <td>&& ||</td>
                    <td>AND and OR Operators</td>
                    <td>$age >= 18 && $score >= 680 || $state == 'NC'</td>
                    <td><?php $age = 18; $score=650; $state='FL'; ?>$age >= 18 && $score >= 680 || $state == 'NC': is <?php echo ($age >= 18 && $score >= 680 || $state == 'NC') ? 'true' : 'false'; ?></td>
                </tr>               
                <tr>
                    <td>&& || !</td>
                    <td>AND, OR, and NOT Operators</td>
                    <td>$old_customer || $loan_amount >= 10000 && $score < $min_score + 200</td>
                    <td><?php $old_customer = true; $loan_amount=10000; $score=650; $min_score=200; ?>$old_customer || $loan_amount >= 10000 && $score < $min_score + 200: is <?php echo ($old_customer || $loan_amount >= 10000 && $score < $min_score + 200) ? 'true' : 'false'; ?></td>
                </tr>               
                <tr>
                    <td>()</td>
                    <td>How parentheses can change the evaluation</td>
                    <td>($old_customer || $loan_amount >= 10000) && $score < $min_score + 200</td>
                    <td><?php $old_customer = true; $loan_amount=10000; $score=650; $min_score=200; ?>($old_customer || $loan_amount >= 10000) && $score < $min_score + 200: is <?php echo (($old_customer || $loan_amount >= 10000) && $score < $min_score + 200) ? 'true' : 'false'; ?></td>
                </tr>
            </table>
        </article>
                
        <!--- Selection Structures : if/else/elseif --->
        <article>
            <h3>Selection Structures</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>An if clause with one statement and no braces</td>
                    <td>if(!isset($rate)) $rate = 0.075</td>
                    <td>$rate is: <?php if(!isset($rate)) $rate = 0.075; echo $rate; ?></td>
                </tr>
                <tr>
                    <td>An if clause with one statement and braces</td>
                    <td>if($qualified) { echo 'You qualify for enrollment.'; ?></td>
                    <td><?php $qualified = true; if($qualified) { echo 'You qualify for enrollment.';} ?></td>
                </tr>
                <tr>
                    <td>If and else clause with one statement each and no braces</td>
                    <td>if($age >= 18) 
                            echo 'You may vote.'
                        else
                            echo 'You may not vote.';
                            $may_vote = false;</td>
                    <td><?php $age = 18; 
                        if($age >= 18) 
                            echo 'You may vote.';
                        else
                            echo 'You may not vote.';
                            $may_vote = false; ?></td>
                </tr>
                <tr>
                    <td>Braces make your code easier to modify or enhance</td>
                    <td>if($score >= 680){
                            echo 'Your loan is approved.'
                        } else {
                            echo 'Your loan is not approved.';
                        }
                    </td>
                    <td><?php $score = 680; 
                        if($score >= 680){
                            echo 'Your loan is approved.';
                        } else {
                            echo 'Your loan is not approved.';
                        } ?></td>
                </tr>
                <tr>
                    <td>A nested if statement to determine if a year is a leap year</td>
                    <td>$is_leap_year = false;
                        if($year % 4 == 0){
                            if($year % 100 == 0){
                                if($year % 400 == 0){
                                    $is_leap_year = true;
                                } else {
                                    $is_leap_year = false;
                            } else {
                                $is_leap_year = true;
                        } else {
                            $is_leap_year = false;
                        }
                        echo $is_leap_year;
                    </td>
                    <td><?php 
                        $year = 2017;
                        $is_leap_year = false;
                        if($year % 4 == 0){
                            if($year % 100 == 0){
                                if($year % 400 == 0){
                                    $is_leap_year = true;
                                } else {
                                    $is_leap_year = false;
                                }
                            } else {
                                $is_leap_year = true;
                            }
                        } else {
                            $is_leap_year = false;
                        }
                        echo ($is_leap_year) ? 'true' : 'false'; ?></td>
                </tr>
            </table>
        </article>
        
        <!---Conditional Operator--->
        <article>
            <h3>Selection Structures</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>An if statement with one else if clause</td>
                    <td>if ($age < 18) {
                            echo "You're too younge for a loan.";
                        } else if ($score < 680) {
                            echo "Your credit score is too low for a loan.";
                        }</td>
                    <td><?php 
                        $score = 500;
                        if ($age < 18) {
                            echo "You're too younge for a loan.";
                        } else if ($score < 680) {
                            echo "Your credit score is too low for a loan.";
                        }?></td>
                </tr>
                <tr>
                    <td>An if statement with an else if and an else clause</td>
                    <td>if ($age < 18) {
                            echo "You're too younge for a loan.";
                        } else if ($score < 680) {
                            echo "Your credit score is too low for a loan.";
                        } else {
                            echo "Your approved for a loan.";
                        }</td>
                    <td><?php 
                        if ($age < 18) {
                            echo "You're too younge for a loan.";
                        } else if ($score < 680) {
                            echo "Your credit score is too low for a loan.";
                        } else {
                            echo "Your approved for a loan.";
                        }?></td>
                </tr>
                <tr>
                    <td>An if statement with two else if clauses and an else clause</td>
                    <td>
                        $rate_is_valid = false;
                        if (!is_numeric($rate)) {
                            echo "Rate is not a number.";
                        } else if ($rate < 0) {
                            echo "Rate cannot be less than zero.";
                        } else if ($rate > 0.2) {
                            echo "Rate cannot be greater than 20%.";
                        }else {
                            $rate_is_valid = true;
                        }</td>
                    <td><?php 
                        $rate_is_valid = false;
                        if (!is_numeric($rate)) {
                            echo "Rate is not a number.";
                        } else if ($rate < 0) {
                            echo "Rate cannot be less than zero.";
                        } else if ($rate > 0.2) {
                            echo "Rate cannot be greater than 20%.";
                        }else {
                            $rate_is_valid = true;
                            echo ($rate_is_valid) ? 'true' : 'false';
                        } ?></td>
                </tr>
                <tr>
                    <td>An if statement to determine a students letter grade</td>
                    <td>
                        $average = 99;
                        if ($average >= 89.5) {
                            $grade = 'A';
                        } else if ($average >= 79.5) {
                            $grade = 'B';
                        } else if ($average >= 69.5) {
                            $grade = 'C';
                        } else if ($average >= 64.5) {
                            $grade = 'D';
                        } else {
                            $grade = 'F';                        
                        }</td>
                    <td><?php 
                        $average = 99;
                        if ($average >= 89.5) {
                            $grade = 'A';
                        } else if ($average >= 79.5) {
                            $grade = 'B';
                        } else if ($average >= 69.5) {
                            $grade = 'C';
                        } else if ($average >= 64.5) {
                            $grade = 'D';
                        } else {
                            $grade = 'F';                        
                        }
                        echo $grade;?></td>
                </tr>
            </table>
        </article>
        
        <!---Conditional Operator--->
        <article>
            <h3>Conditional Operator</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>Set a string based on a comparison</td>
                    <td>$message = ($age >= 18) ? 'Can Vote' : 'Cannot Vote';</td>
                    <td><?php $message = ($age >= 18) ? 'Can Vote' : 'Cannot Vote'; echo $message; ?></td>
                </tr>
                <tr>
                    <td>Calculate Over Time Pay</td>
                    <td>$overtime = ($age >= 18) ? ($hours - 40) * $rate * 1.5 : 0;</td>
                    <td><?php $hours = 45; $rate = 15.00; $overtime = ($age >= 18) ? ($hours - 40) * $rate * 1.5 : 0; echo $overtime; ?></td>
                </tr>
                <tr>
                    <td>Select a singular or plural ending based on a value</td>
                    <td>
                        $ending = ($error_count ==1) ? '' : 's';
                        $message = 'Found ' . $error_count . 'error' . $ending . '.';
                    </td>
                    <td>
                    <?php 
                        $error_count = 2;
                        $ending = ($error_count == 1) ? '' : 's';
                        $message = 'Found ' . $error_count . ' error' . $ending . '.';
                        echo $message;
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Set a value to 1 if its at a maximum value when adding 1</td>
                    <td>
                        $max_value = 3;
                        $value = 4;
                        $value = ($value >= $max_value) ? 1 : $value + 1;
                        echo $value;
                    </td>
                    <td>
                    <?php 
                        $max_value = 3;
                        $value = 4;
                        $value = ($value >= $max_value) ? 1 : $value + 1;
                        echo $value;
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Return one of two values based on a comparison</td>
                    <td>
                        $number = 3;
                        $highest = 4;
                        return ($number > $highest) ? $highest : $number;
                    </td>
                    <td>
                    <?php 
                        $number = 3;
                        $highest = 4;
                        echo 'Return: ' . ($number > $highest) ? $highest : $number;
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Bound a value within a fixed range</td>
                    <td>
                        $value = 3;
                        $max = 5;
                        $min = 2;
                        $value = ($value > $max) ? $max : (($value < $min) ? $min : $value);
                        echo $value;
                    </td>
                    <td>
                    <?php 
                        $value = 3;
                        $max = 5;
                        $min = 2;
                        $value = ($value > $max) ? $max : (($value < $min) ? $min : $value);
                        echo $value;
                    ?>
                    </td>
                </tr>
            </table>
        </article>
                
        <!---Switch Statements--->
        <article>
            <h3>Switch Statements</h3>
            <form name="frmSwitchStatement" method="post" action="process_form1.php">
                Select CountDown Start Time: 
                <select name="intCountDown">
                    <option value="10" selected="selected">10</option>
                    <option value="10">9</option>
                    <option value="10">8</option>
                    <option value="10">7</option>
                    <option value="10">6</option>
                    <option value="10">5</option>
                    <option value="10">4</option>
                    <option value="10">3</option>
                    <option value="10">2</option>
                    <option value="10">1</option>
                </select>
                <br /><br />
                <button type="submit">Start Count Down</button>
            </form>
        </article>
        
        <!---Iteration Structures--->
        <article>
            <h3>Iteration Structures</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>A while loop that finds the average of 100 random numbers</td>
                    <td>
                        $total = 0;
                        $count = 0;
                        while ($count < 100) {
                            $number = mt_rand(0, 100);
                            $total += $number;
                            $count++;
                        } 
                        $average = $total  / $count;
                        echo 'The average is: ' . $average;
                    </td>
                    <td>
                        <?php
                        $total = 0;
                        $count = 0;
                        while ($count < 100) {
                            $number = mt_rand(0, 100);
                            $total += $number;
                            $count++;
                        } 
                        $average = $total  / $count;
                        echo 'The average is: ' . $average;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>A while loop that finds counts dice until a six is rolled</td>
                    <td>
                        $rolls = 1;
                        while (mt_rand(1,6) != 6) {
                            $rolls++;
                        }
                        echo 'Number of times to roll a six: ' . $rolls;
                    </td>
                    <td>
                        <?php
                        $rolls = 1;
                        while (mt_rand(1,6) != 6) {
                            $rolls++;
                        }
                        echo 'Number of times to roll a six: ' . $rolls;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Nested while loops that get the average and maximum rolls for a six</td>
                    <td>
                        $total = 0;
                        $count = 0;
                        $max = -INF;
                        
                        while ($count < 10000) {
                            $rolls = 1;
                            while (mt_rand(1,6) != 6) {
                                $rolls++;
                            }
                            $total += $rolls;
                            $count++;
                            $max = max($rolls, $max);
                        }
                        $average = $total / $count;
                        echo 'Average: ' . $average . ' Max: ' . $max;
                    </td>
                    <td>
                        <?php
                        $total = 0;
                        $count = 0;
                        $max = -INF;
                        
                        while ($count < 10000) {
                            $rolls = 1;
                            while (mt_rand(1,6) != 6) {
                                $rolls++;
                            }
                            $total += $rolls;
                            $count++;
                            $max = max($rolls, $max);
                        }
                        $average = $total / $count;
                        echo 'Average: ' . $average . ' Max: ' . $max;
                        ?>
                    </td>
                </tr>
            </table>
        </article>
        
         <!---do-while Loops--->
        <article>
            <h3>Do-While Loops</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>A do-while loop that counts dice rolls until a six is rolled</td>
                    <td>
                        $rolls = 0;
                        do {
                            $rolls++;
                        } while (mt_rand(1,6) != 6);
                        echo 'Number of times to roll a six: ' . $rolls;
                    </td>
                    <td>
                        <?php
                        $rolls = 0;
                        do {
                            $rolls++;
                        } while (mt_rand(1,6) != 6);
                        echo 'Number of times to roll a six: ' . $rolls;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>A do-while loop to find the max and min of 10 random values</td>
                    <td>
                        $max = -INF;
                        $min = INF;
                        $count = 0;
                        do {
                            $number = mt_rand(0, 100);
                            $max = max($max, $number);
                            $min = min($max, $number);
                            $count++;
                        } while ($count < 10);
                        echo 'Max: ' . $max . ' Min: ' . $min; 
                    </td>
                    <td>
                        <?php
                        $max = -INF;
                        $min = INF;
                        $count = 0;
                        do {
                            $number = mt_rand(0, 100);
                            $max = max($max, $number);
                            $min = min($max, $number);
                            $count++;
                        } while ($count < 10);
                        echo 'Max: ' . $max . ' Min: ' . $min; 
                        ?>
                    </td>
                </tr>
            </table>
        </article>
               
        <!---for Loops--->
        <article>
            <h3>For Loops</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>A for loop to display even numbers from 2 to 10</td>
                    <td>
                        for ($number = 2; $number <= 10; $number +=2;){
                            echo $number . '<br>';
                        }
                    </td>
                    <td>
                        <?php
                        for ($number = 2; $number <= 10; $number += 2){
                            echo $number . '<br>';
                        }
                        ?> 
                    </td>
                </tr>
                <tr>
                    <td>A for loop to display all the factors of a number</td>
                    <td>
                        $number = 18;
                        for ($i = 1; $i < $number; $i++) {
                            if($number % $i == 0) {
                                echo $i . ' is a factor of ' . $number . '<br>';
                            }
                        }
                    </td>
                    <td>
                        <?php
                        $number = 18;
                        for ($i = 1; $i < $number; $i++) {
                            if($number % $i == 0) {
                                echo $i . ' is a factor of ' . $number . '<br>';
                            }
                        }
                        ?> 
                    </td>
                </tr>
                <tr>
                    <td>A for loop that uses the alternative syntax to display a drop-down list</td>
                    <td>
                        <label>Interest Rate: </label>
                        <select name="rate">
                        < ? php for ($v = 5; $v <= 12; $v++) : ?>
                            <option value="< ? php echo $v; ?>">
                                < ? php echo $v; ?>
                            </option>
                        < ? php endfor; ?>                              
                        </select>
                        <br>
                    </td>
                    <td>
                        <label>Interest Rate: </label>
                        <select name="rate">
                        <?php for ($v = 5; $v <= 12; $v++) : ?>
                            <option value="<?php echo $v; ?>">
                                <?php echo $v; ?>
                            </option>
                        <?php endfor; ?>                              
                        </select>
                        <br>
                    </td>
                </tr>
            </table>
        </article>
        
        <!---for Loops--->
        <article>
            <h3>Break and Continue Statements</h3>
            <table>
                <tr>
                    <th>Description</th>
                    <th>Example</th>
                    <th>Implementation</th>
                </tr>
                <tr>
                    <td>The Break statement in a while loop</td>
                    <td>
                        while(true) {
                            $number = mt_rand(1,10);
                            if($number % 2 == 0) {
                                break;
                            }
                        }
                        echo $number;
                    </td>
                    <td>
                        <?php
                        while(true) {
                            $number = mt_rand(1,10);
                            if($number % 2 == 0) {
                                break;
                            }
                        }
                        echo $number;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>The Break statement in a for loop</td>
                    <td>
                        $number = 13;
                        $prime = true;
                        for ($i = 2; $i < $number; $i++) {
                            if ($number % $i == 0) {
                                $prime = false;
                                break;
                            }
                        }
                    </td>
                    <td>
                        <?php
                        $number = 13;
                        $prime = true;
                        for ($i = 2; $i < $number; $i++) {
                            if ($number % $i == 0) {
                                $prime = false;
                                break;
                            }
                        }
                        echo $prime;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>The Continue statement in a for loop</td>
                    <td>
                        for($number = 1; $number <= 10; $number++) {
                            if($number % 3 == 0) {
                                continue;
                            }
                            echo $number . '<br>';
                        }
                    </td>
                    <td>
                        <?php
                        for($number = 1; $number <= 10; $number++) {
                            if($number % 3 == 0) {
                                continue;
                            }
                            echo $number . '<br>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>The Continue statement in a while loop</td>
                    <td>
                        $number = 1;
                        while($number <= 10) {
                            if($number % 3 == 0) {
                                $number++;
                                continue;
                            }
                            echo $number . '<br>';
                            $number++;
                        }
                    </td>
                    <td>
                        <?php
                        $number = 1;
                        while($number <= 10) {
                            if($number % 3 == 0) {
                                $number++;
                                continue;
                            }
                            echo $number . '<br>';
                            $number++;
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </article>
    </section>
</main>
<?php include 'footer.php'; ?>